/*
 * utils.c
 *
 *  Created on: Mar 15, 2022
 *      Author: ManuelVr
 */
#include "main.h"
#include "utils.h"



const char * off_str = "Off";



void I8Clip(int8_t * p_value, int16_t min, int16_t max)
{
	if (*p_value > max) *p_value = max;
	else if (*p_value < min) *p_value = min;
}

void U8Clip(uint8_t * p_value, int16_t min, int16_t max)
{
	if (*p_value > max) *p_value = max;
	else if (*p_value < min) *p_value = min;
}

void I16Clip(int16_t * p_value, int16_t min, int16_t max)
{
	if (*p_value > max) *p_value = max;
	else if (*p_value < min) *p_value = min;
}

void U16Clip(uint16_t * p_value, int16_t min, int16_t max)
{
	if (*p_value > max) *p_value = max;
	else if (*p_value < min) *p_value = min;
}

void I8ClipInc(int8_t * p_value, int16_t inc, int16_t min, int16_t max)
{
	int16_t value = *p_value + inc;
	if (value > max) value = max;
	else if (value < min) value = min;
	*p_value = (int8_t)value;
}

void I16ClipInc(int16_t * p_value, int16_t inc, int16_t min, int16_t max)
{
	int16_t value = *p_value + inc;
	if (value > max) value = max;
	else if (value < min) value = min;
	*p_value = value;
}

void U8ClipInc(uint8_t * p_value, int16_t inc, int16_t min, int16_t max)
{
	int16_t value = *p_value + inc;
	if (value > max) value = max;
	else if (value < min) value = min;
	*p_value = (uint8_t)value;
}


uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


template<typename DataType>
void InsertionSort(DataType arr[], int n) {
	int i, j;
	DataType key;

	for (i = 1; i < n; i++) {
		key = arr[i];
		j = i - 1;

		while (j >= 0 && arr[j] > key) {
			arr[j + 1] = arr[j];
			j = j - 1;
		}
		arr[j + 1] = key;
	}
}

template<typename DataType>
void BubbleSort(DataType arr[], int n) {
	int i, j;
	for (i = 0; i < n-1; i++) { 		//Loop for ascending ordering
		for (j = 0; j < n-i-1; j++) { 	//Loop for comparing other values
			if (arr[j] > arr[j+1]) {  	//Comparing other array elements
				DataType temp = arr[j]; //Using temporary variable for storing last value
				arr[j] = arr[j+1];		//replacing value
				arr[j+1] = temp;		//storing last value
			}
		}
	}
}


void U8Sort(uint8_t * array, int len)
{
	InsertionSort(array, len);
}

