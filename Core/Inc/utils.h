/*
 * utils.h
 *
 *  Created on: Mar 15, 2022
 *      Author: ManuelVr
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#pragma once

#include "stm32f4xx_hal.h"

#define  MIN(a, b)      (((a) < (b)) ? (a) : (b))
#define  MAX(a, b)      (((a) > (b)) ? (a) : (b))

#define CLIP16(a, b, c)		I16Clip(&a, b, c);
#define CONSTRAIN(var, min, max) \
  if (var < (min)) { \
    var = (min); \
  } else if (var > (max)) { \
    var = (max); \
  }

#define CLIP_NOTE(note) \
  while(note> NOTE_MAX) {note-=12;}



extern const char * off_str;


void I8Clip(int8_t * p_value, int16_t min, int16_t max);
void U8Clip(uint8_t * p_value, int16_t min, int16_t max);
void I16Clip(int16_t * p_value, int16_t min, int16_t max);
void U16Clip(uint16_t * p_value, int16_t min, int16_t max);

void I8ClipInc(int8_t * p_value, int16_t inc, int16_t min, int16_t max);
void I16ClipInc(int16_t * p_value, int16_t inc, int16_t min, int16_t max);

void U8ClipInc(uint8_t * p_value, int16_t inc, int16_t min, int16_t max);

uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max);

void U8Sort(uint8_t * array, int len);

#ifdef __cplusplus
}
#endif


#endif /* SRC_UTILS_H_ */
