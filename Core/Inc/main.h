/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdbool.h"
#include "stdlib.h"
#include "string.h"
#include "error.h"
#include "utils.h"


#ifdef DEBUG


#define SEGGER_RTT
//#define TRACE_ITM



#ifdef SEGGER_RTT
#include "SEGGER_RTT.h"
#endif


//#define SEMIHOSTING

#endif
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/**********************************
 * 		HW VERSION SELECTION
 *********************************/

#define PROTO_V5


/*********************************/

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define TIMESTAMP_MAX 0xFFFFFFFF

#define BOOT_MAGIC_WORD 0xDEADBEEF

extern volatile uint32_t app_ready;

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#ifdef SEMIHOSTING
#ifdef TRACE
#define LOG_INFO(...) \
		if ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) \
				==CoreDebug_DHCSR_C_DEBUGEN_Msk) { \
			trace_printf(__VA_ARGS__); \
		}
#else
#define LOG_INFO(...) \
		if ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) \
				==CoreDebug_DHCSR_C_DEBUGEN_Msk) { \
			printf(__VA_ARGS__); \
		}
#endif
#elif (defined SEGGER_RTT)
#define LOG_INFO(...)	\
		if ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) \
				==CoreDebug_DHCSR_C_DEBUGEN_Msk) { \
			SEGGER_RTT_printf(0, __VA_ARGS__); \
		}
#elif (defined TRACE_ITM)

extern void initialise_monitor_handles(void);


#define LOG_INFO(...)	\
		if ((CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) \
				==CoreDebug_DHCSR_C_DEBUGEN_Msk) { \
			printf(__VA_ARGS__); \
		}
#else
#define LOG_INFO(...)	((void)0U)
#endif

#ifdef SEMIHOSTING
#define LOG_SCANF(...)	scanf(__VA_ARGS__)
#else
#define LOG_SCANF(...)	((void)0U)
#endif

#define LOG_TS()	LOG_INFO("TS: %d ", uwTick);
#define LOG_ENDL()	LOG_INFO("\r\n");

/* Absolute value */
#define ABS(x)   ((x) > 0 ? (x) : -(x))


/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(ERR_err_e error);

/* USER CODE BEGIN EFP */
void  elapsed_time_clr   (uint32_t  i);      // Clear measured values
//void  elapsed_time_init  (void);             // Module initialization
void  elapsed_time_start (uint32_t  i);      // Start measurement
void  elapsed_time_stop  (uint32_t  i);      // Stop  measurement
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define OLED_DC_Pin GPIO_PIN_13
#define OLED_DC_GPIO_Port GPIOC
#define OLED_CS_Pin GPIO_PIN_14
#define OLED_CS_GPIO_Port GPIOC
#define OLED_RES_Pin GPIO_PIN_15
#define OLED_RES_GPIO_Port GPIOC
#define OUT_STH_Pin GPIO_PIN_0
#define OUT_STH_GPIO_Port GPIOC
#define INOKB_CHR_Pin GPIO_PIN_1
#define INOKB_CHR_GPIO_Port GPIOC
#define START_STOP_CV_IN_Pin GPIO_PIN_0
#define START_STOP_CV_IN_GPIO_Port GPIOA
#define WS2812D_Pin GPIO_PIN_1
#define WS2812D_GPIO_Port GPIOA
#define LATCH_Pin GPIO_PIN_2
#define LATCH_GPIO_Port GPIOA
#define CS_RCLK_Pin GPIO_PIN_4
#define CS_RCLK_GPIO_Port GPIOA
#define EEPROM_CS_Pin GPIO_PIN_4
#define EEPROM_CS_GPIO_Port GPIOC
#define IN_BTN_PRESSED_Pin GPIO_PIN_5
#define IN_BTN_PRESSED_GPIO_Port GPIOC
#define IN_MUTE_BTN_Pin GPIO_PIN_0
#define IN_MUTE_BTN_GPIO_Port GPIOB
#define ADC_BAT_Pin GPIO_PIN_1
#define ADC_BAT_GPIO_Port GPIOB
#define SHT_10V_Pin GPIO_PIN_12
#define SHT_10V_GPIO_Port GPIOB
#define STAT_CHR_Pin GPIO_PIN_14
#define STAT_CHR_GPIO_Port GPIOB
#define ENBST_CHR_Pin GPIO_PIN_15
#define ENBST_CHR_GPIO_Port GPIOB
#define START_STOP_OUT_Pin GPIO_PIN_6
#define START_STOP_OUT_GPIO_Port GPIOC
#define CLOCK_OUT_Pin GPIO_PIN_7
#define CLOCK_OUT_GPIO_Port GPIOC
#define CLOCK_IN_Pin GPIO_PIN_8
#define CLOCK_IN_GPIO_Port GPIOC
#define CLOCK_IN_EXTI_IRQn EXTI9_5_IRQn
#define BLE_RESET_Pin GPIO_PIN_4
#define BLE_RESET_GPIO_Port GPIOB
#define BLE_BOOT_Pin GPIO_PIN_9
#define BLE_BOOT_GPIO_Port GPIOC
#define DRUMS_EN_Pin GPIO_PIN_8
#define DRUMS_EN_GPIO_Port GPIOA
#define TM1638_CS_Pin GPIO_PIN_15
#define TM1638_CS_GPIO_Port GPIOA
#define MUX_USB_Pin GPIO_PIN_2
#define MUX_USB_GPIO_Port GPIOD
//#define ENB_MIDI_SUPPLY_Pin GPIO_PIN_4
//#define ENB_MIDI_SUPPLY_GPIO_Port GPIOB
//#define OCB_MIDI_SUPPLY_Pin GPIO_PIN_5
//#define OCB_MIDI_SUPPLY_GPIO_Port GPIOB
#define GATES_10V_5V_Pin GPIO_PIN_9
#define GATES_10V_5V_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
typedef enum
{
	RETURN_OK = 1,
	RETURN_NOK = 0,
} RETURN_e;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
