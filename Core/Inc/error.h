/*
 * error.h
 *
 *  Created on: Aug 4, 2021
 *      Author: ManuelVr
 */

#ifndef INC_ERROR_H_
#define INC_ERROR_H_

typedef enum
{
	err_hard_fault = 0,
	err_stack,
	err_cpu,
	err_multipres_vel_edit,
	err_multipres_gate_edit,
	err_pad_debounce,
	err_function_pad_debounce,
	err_matrix_pad_debounce,
	err_spi1,
	err_dma_tim2,
	err_dma_tim2_init,
	err_i2c1_init,
	err_i2c2_init,
	err_spi1_init,
	err_spi2_init,
	err_spi3_init,
	err_tim2_init,
	err_tim3_init,
	err_uart_init,
	err_rrc_init,
	err_adc_init,
	err_crc_init,
	err_pcd_init,
	err_usbd_ll_init,
	err_usbd_init,
	err_usbd_register_class,
	err_usbd_register_interface,
	err_usbd_start,
	err_get_step_note,
	err_chord_type_update_offset,
	err_chord_type_select,
	err_chord_inversion_select,
	err_ui,
	err_ui_callback,
	err_seq_index,
	err_seq_move,
	err_seq_copy_step,
	err_seq_calc_step_note,
	err_seq_update_matrix_longpress,
	err_seq_transport_mono,
	err_seq_transport_repeat,
	err_seq_transport_repeat_poly,
	err_seq_transport_legato,
	err_flash_startup,
	err_flash_save_calib_sync,
	err_flash_load_calib_sync,
	err_flash_timeout,
	err_flash_load_pad,
	err_flash_save_pad,
	err_preset_load,
	err_preset_save,
	err_load_seq_async,
	err_mspi_new_transfer,
	err_midi_tim_init,
	err_midi_task,
	err_midi_send_noteoff,
	err_midi_send_noteon,
	err_midi_uart,
	err_midi_usb_rx,
	err_midi_usb_tx,
	err_mspi_trigger,
	err_lcd_init,
	err_lcd_trigger,
	err_lcd_tempmenu_title,
	err_lcd_tempmenu_txt,
	err_lcd_pointer_draw,
	err_lcd_puts,
	err_lcd_putsize,
	err_lcd_tempval_txt1,
	err_lcd_tempval_txt2,
	err_lcd_draw_root,
	err_lcd_draw_sync,
	err_lcd_draw_div,
	err_lcd_draw_tripp,
	err_lcd_draw_scale,
	err_lcd_draw_swing,
	err_lcd_draw_direction,
	err_cv_init,
	err_cv_load_calib,
	err_cv_save_calib,
	err_cv_dac_tx,
	err_cv_draw,
	err_cv_voice,
	err_arp,
	err_arp_type,
	err_keyboard,
	err_rec_gate_tie,
	err_rec_gate_neg,
	err_rec_note,
	err_rec_stack,
	err_rec_mono_step,
	err_bpm_div,
	err_arr_pattern,
	err_arr_state,
	err_project,
	err_preset,
	err_seq_loop,
	err_leds_offset,
	err_leds_fade,
	err_scale,
	err_scale_new,
	err_poly_step,
	err_poly_stack_index,
	err_poly_rand,
	err_sysex_data,
	err_mod_idx,
	err_harmonizer,
	ERR_SIZE
} ERR_err_e;

#endif /* INC_ERROR_H_ */
