/*
 * Version.h
 *
 *  Created on: Oct 18, 2022
 *      Author: ManuelVr
 */

#ifndef INC_VERSION_H_
#define INC_VERSION_H_

#ifdef __cplusplus
extern "C" {
#endif


/*======================================================*/
/* 					SERIAL NUMBER						*/
/*======================================================*/
#ifdef PROTO_V5
#define HW_VERSION "HW: V5"
#endif

//#warning "\n UPDATE SW VERSION!!!!!! \n"

#ifdef DEBUG
#define SW_VERSION "SW: v3.1.4 DEB"
#else
#define SW_VERSION "SW: v3.1.4"
#endif





#define         DEVICE_ID1          (UID_BASE)
#define         DEVICE_ID1_         (UID_BASE + 0x2)
#define         DEVICE_ID2          (UID_BASE + 0x4)
#define         DEVICE_ID2_         (UID_BASE + 0x6)
#define         DEVICE_ID3          (UID_BASE + 0x8)
#define         DEVICE_ID3_         (UID_BASE + 10)


#ifdef __cplusplus
}
#endif

#endif /* INC_VERSION_H_ */
