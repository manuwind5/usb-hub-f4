/**
 ******************************************************************************
 * @file    usbd_desc.c
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    19-September-2011
 * @brief   This file provides the USBD descriptors and string formating method.
 ******************************************************************************
 * @attention
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "usbd_core.h"
#include "usbd_desc.h"
#include "usbd_req.h"
#include "usbd_conf.h"
#include "usb_regs.h"

#include "main.h"
#include "Version.h"


#include "usbd_cdc_core.h"

/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
 * @{
 */


/** @defgroup USBD_DESC 
 * @brief USBD descriptors module
 * @{
 */

/** @defgroup USBD_DESC_Private_TypesDefinitions
 * @{
 */
/**
 * @}
 */


/** @defgroup USBD_DESC_Private_Defines
 * @{
 */


/** @defgroup USB_String_Descriptors
 * @{
 */
#define USBD_MIDI_VID						0x28E9
#define USBD_MIDI_PID_FS					0x1111
#define USBD_MIDI_MANUFACTURER_STRING     	"OXI Instruments"
#define USBD_MIDI_PRODUCT_FS_STRING          "OXI ONE"
#define USBD_MIDI_CONFIGURATION_FS_STRING    "MIDI Config"
#define USBD_MIDI_INTERFACE_FS_STRING        "MIDI Interface"


#define USBD_CDC_VID                      	0x28E9
#define USBD_CDC_PID_FS                   	0x1111
#define USBD_CDC_MANUFACTURER_STRING     	"monome"
#define USBD_CDC_PRODUCT_FS_STRING     		"monome"
#define USBD_CDC_CONFIGURATION_FS_STRING  	"CDC Config"
#define USBD_CDC_INTERFACE_FS_STRING      	"CDC Interface"

#define USBD_LANGID_STRING      		1033 // CharSet // U.S.

/**
 * @}
 */


/** @defgroup USBD_DESC_Private_Macros
 * @{
 */
/**
 * @}
 */


/** @defgroup USBD_DESC_Private_Variables
 * @{
 */

const USBD_DEVICE USR_MIDI_desc =
{
		USBD_USR_MIDI_DeviceDescriptor,
		USBD_USR_LangIDStrDescriptor,
		USBD_USR_MIDI_ManufacturerStrDescriptor,
		USBD_USR_MIDI_ProductStrDescriptor,
		USBD_USR_MIDI_SerialStrDescriptor,
		USBD_USR_MIDI_ConfigStrDescriptor,
		USBD_USR_MIDI_InterfaceStrDescriptor,

};


const USBD_DEVICE USR_CDC_desc =
{
		USBD_USR_CDC_DeviceDescriptor,
		USBD_USR_LangIDStrDescriptor,
		USBD_USR_CDC_ManufacturerStrDescriptor,
		USBD_USR_CDC_ProductStrDescriptor,
		USBD_USR_CDC_SerialStrDescriptor,
		USBD_USR_CDC_ConfigStrDescriptor,
		USBD_USR_CDC_InterfaceStrDescriptor,

};

const USBD_DEVICE USR_CDC_oxi_desc =
{
		USBD_USR_CDC_DeviceDescriptor,
		USBD_USR_LangIDStrDescriptor,
		USBD_USR_MIDI_ManufacturerStrDescriptor,
		USBD_USR_MIDI_ProductStrDescriptor,
		USBD_USR_MIDI_SerialStrDescriptor,
		USBD_USR_CDC_ConfigStrDescriptor,
		USBD_USR_CDC_InterfaceStrDescriptor,

};

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
#if defined ( __ICCARM__ ) /*!< IAR Compiler */
#pragma data_alignment=4
#endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */

__ALIGN_BEGIN const uint8_t USBD_MIDI_DeviceDesc[USB_LEN_DEV_DESC] __ALIGN_END =
{
		0x12,                       /*bLength */
		USB_DESC_TYPE_DEVICE,       /*bDescriptorType*/
		0x00,                       /*bcdUSB */
		0x01,
		0x00,                       /*bDeviceClass*/
		0x00,                       /*bDeviceSubClass*/
		0x00,                       /*bDeviceProtocol*/
		USB_OTG_MAX_EP0_SIZE,           /*bMaxPacketSize*/
		LOBYTE(USBD_MIDI_VID),           /*idVendor*/
		HIBYTE(USBD_MIDI_VID),           /*idVendor*/
		LOBYTE(USBD_MIDI_PID_FS),        /*idProduct*/
		HIBYTE(USBD_MIDI_PID_FS),        /*idProduct*/
		0x00,                       /*bcdDevice rel. 2.00*/
		0x01,
		USBD_IDX_MFC_STR,           /*Index of manufacturer  string*/
		USBD_IDX_PRODUCT_STR,       /*Index of product string*/
		USBD_IDX_SERIAL_STR,        /*Index of serial number string*/
		USBD_CFG_MAX_NUM  /*bNumConfigurations*/
};

__ALIGN_BEGIN const uint8_t USBD_CDC_DeviceDesc[USB_SIZ_DEVICE_DESC] __ALIGN_END =
{
		0x12,                       /*bLength */
		USB_DESC_TYPE_DEVICE, 		/*bDescriptorType*/
		0x00,                       /*bcdUSB */
		0x02,
		DEVICE_CLASS_CDC,                       /*bDeviceClass*/
		DEVICE_SUBCLASS_CDC,                       /*bDeviceSubClass*/
		0x00,                       /*bDeviceProtocol*/
		USB_OTG_MAX_EP0_SIZE,      /*bMaxPacketSize*/
		LOBYTE(USBD_CDC_VID),           /*idVendor*/
		HIBYTE(USBD_CDC_VID),           /*idVendor*/
		LOBYTE(USBD_CDC_PID_FS),           /*idVendor*/
		HIBYTE(USBD_CDC_PID_FS),           /*idVendor*/
		0x00,                       /*bcdDevice rel. 2.00*/
		0x02,
		USBD_IDX_MFC_STR,           /*Index of manufacturer  string*/
		USBD_IDX_PRODUCT_STR,       /*Index of product string*/
		USBD_IDX_SERIAL_STR,        /*Index of serial number string*/
		USBD_CFG_MAX_NUM            /*bNumConfigurations*/
} ; /* USB_DeviceDescriptor */


/* USB Standard Device Descriptor */
__ALIGN_BEGIN const uint8_t USBD_LangIDDesc[USB_SIZ_STRING_LANGID] __ALIGN_END =
{
		USB_SIZ_STRING_LANGID,
		USB_DESC_TYPE_STRING,
		LOBYTE(USBD_LANGID_STRING),
		HIBYTE(USBD_LANGID_STRING),
};
/**
 * @}
 */


/** @defgroup USBD_DESC_Private_FunctionPrototypes
 * @{
 */
/**
 * @}
 */


/** @defgroup USBD_DESC_Private_Functions
 * @{
 */

/**
 * @brief  USBD_USR_DeviceDescriptor
 *         return the device descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
const uint8_t *  USBD_USR_MIDI_DeviceDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	*length = sizeof(USBD_MIDI_DeviceDesc);
	return USBD_MIDI_DeviceDesc;
}

const uint8_t *  USBD_USR_CDC_DeviceDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	*length = sizeof(USBD_CDC_DeviceDesc);
	return USBD_CDC_DeviceDesc;
}

/**
 * @brief  USBD_USR_LangIDStrDescriptor
 *         return the LangID string descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
const uint8_t *  USBD_USR_LangIDStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	*length =  sizeof(USBD_LangIDDesc);
	return USBD_LangIDDesc;
}


/**
 * @brief  USBD_USR_ProductStrDescriptor
 *         return the product string descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
const uint8_t *  USBD_USR_MIDI_ProductStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_MIDI_PRODUCT_FS_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}

const uint8_t *  USBD_USR_CDC_ProductStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_CDC_PRODUCT_FS_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}

/**
 * @brief  USBD_USR_ManufacturerStrDescriptor
 *         return the manufacturer string descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
const uint8_t *  USBD_USR_MIDI_ManufacturerStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_MIDI_MANUFACTURER_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}

const uint8_t *  USBD_USR_CDC_ManufacturerStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_CDC_MANUFACTURER_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}


/**
 * @brief  USBD_USR_SerialStrDescriptor
 *         return the serial number string descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
const uint8_t *  USBD_USR_MIDI_SerialStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	uint16_t deviceserial0, deviceserial1;
	uint16_t deviceserial0_, deviceserial1_;

	deviceserial0 = *(uint16_t *) DEVICE_ID1;
	deviceserial1 = *(uint16_t *) DEVICE_ID2;
//	deviceserial2 = *(uint16_t *) DEVICE_ID3;
	deviceserial0_ = *(uint16_t *) DEVICE_ID1_;
	deviceserial1_ = *(uint16_t *) DEVICE_ID2_;
//	deviceserial2_ = *(uint16_t *) DEVICE_ID3_;

	char temp [USB_MAX_STR_DESC_SIZ];

	snprintf(temp, USB_MAX_STR_DESC_SIZ, "%08x%08x%08x%08x", deviceserial1_, deviceserial1, deviceserial0_, deviceserial0);

	USBD_GetString ((uint8_t *)temp, USBD_StrDesc, length);
	return (const uint8_t * )USBD_StrDesc;
}



#define SERIAL_NUM_UNICODE_MONOME  {'m', 0,'1', 0,'0', 0,'0', 0,'0', 0,'0', 0,'0', 0,'0',0}
#define SERIAL_MONOME_LEN 8
#define  USB_SIZ_STRING_SERIAL       0x1A

const uint8_t *  USBD_USR_CDC_SerialStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	const char serial[] = SERIAL_NUM_UNICODE_MONOME;
	*length = 2 + SERIAL_MONOME_LEN * 2;
	*length = USB_SIZ_STRING_SERIAL;
	memcpy(&USBD_StrDesc[2], serial, 16);
	return (const uint8_t * )USBD_StrDesc;
}

//#define SERIAL_NUM_MONOME  "m0000000"
//const uint8_t *  USBD_USR_CDC_SerialStrDescriptor( uint8_t speed , uint16_t *length)
//{
//	UNUSED(speed);
//	USBD_GetString ((uint8_t*)SERIAL_NUM_MONOME, USBD_StrDesc, length);
//	return (const uint8_t * )USBD_StrDesc;
//}

/**
 * @brief  USBD_USR_ConfigStrDescriptor
 *         return the configuration string descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
uint8_t *  USBD_USR_MIDI_ConfigStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_MIDI_CONFIGURATION_FS_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}

uint8_t *  USBD_USR_CDC_ConfigStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_CDC_CONFIGURATION_FS_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}


/**
 * @brief  USBD_USR_InterfaceStrDescriptor
 *         return the interface string descriptor
 * @param  speed : current device speed
 * @param  length : pointer to data length variable
 * @retval pointer to descriptor buffer
 */
uint8_t *  USBD_USR_MIDI_InterfaceStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_MIDI_INTERFACE_FS_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}

uint8_t *  USBD_USR_CDC_InterfaceStrDescriptor( uint8_t speed , uint16_t *length)
{
	UNUSED(speed);
	USBD_GetString ((uint8_t*)USBD_CDC_INTERFACE_FS_STRING, USBD_StrDesc, length);
	return USBD_StrDesc;
}

/**
 * @}
 */


/**
 * @}
 */


/**
 * @}
 */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/

