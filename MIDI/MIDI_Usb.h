/*
 * MIDI_Usb.h
 *
 *  Created on: Jun 21, 2021
 *      Author: ManuelVr
 */

#ifndef MIDI_USB_H_
#define MIDI_USB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

void SYSEX_SendUSBSinglePackage(uint8_t * p_data, int16_t length, uint16_t last);
void SYSEX_SendUSB(uint8_t * p_data, int16_t length);
void MIDI_ProcessUSBRxMessage(void);


#ifdef __cplusplus
}
#endif


#endif /* MIDI_USB_H_ */
