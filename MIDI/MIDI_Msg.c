/*
 * MIDI_Msg.c
 *
 *  Created on: Dec 5, 2019
 *      Author: ManuelVr
 */

#include <MIDI_queue.h>
#include <MIDI_Task.h>
#include "usbd_midi_user.h"
#include "MIDI_Msg.h"

#define MIDI_DEVICE_ID	0x7F

static uint8_t buffer[4];

static void MidiSendUSBMessage(void)
{
	if ((buffer[0] & 0xE0) != 0) return; // To filter cable numbers different than 0 and 1
	MIDI_USBDataTx(buffer, 4);
}

static void MidiSendOutput(uint16_t size, uint16_t filter)
{
	MidiSendUSBMessage();
}

void MIDI_SendNoteOn(uint8_t ch, int8_t note, uint8_t vel)
{
	buffer[0] = 0x09 + (ch & 0xf0);
	buffer[1] = NoteOn | (ch & 0xf);
	buffer[2] = 0x7f & note;
	buffer[3] = 0x7f & vel;

	MidiSendOutput(3, 100);
}


void MIDI_SendStop(void)
{
	buffer[0] = 0x0f;
	buffer[1] = MIDI_STOP_MSG;
	buffer[2] = 0;
	buffer[3] = 0;

	MidiSendUSBMessage();
}

void MIDI_SendStart(void)
{
	buffer[0] = 0x0f;
	buffer[1] = MIDI_START_MSG;
	buffer[2] = 0;
	buffer[3] = 0;

	MidiSendUSBMessage();
}

void MIDI_SendContinue(void)
{
	buffer[0] = 0x0f;
	buffer[1] = MIDI_CONTINUE_MSG;
	buffer[2] = 0;
	buffer[3] = 0;

	MidiSendUSBMessage();
}

void MIDI_SendClk(void)
{
	buffer[0] = 0x0f;
	buffer[1] = MIDI_CLOCK_MSG;
	buffer[2] = 0;
	buffer[3] = 0;

	MidiSendUSBMessage();
}
