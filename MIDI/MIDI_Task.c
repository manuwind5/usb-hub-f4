/********************************************************

         @@@@@@@@@@           @@@@@@@@@@@@@@@@@@@@@@
     @@@@          @@@
   @@@               @@@      @@@                @@@
  @@                   @@       @@@            @@@
 @@@                    @@         @@@      @@@
 @@          @@         @@            @@  @@
 @@@                    @@             @@@@
  @@                   @@            @@@  @@@
   @@@               @@@          @@@        @@@
     @@@@          @@@         @@@             @@@
         @@@@@@@@@@           @@                 @@@

 *********************************************************/
/*
 *  Created on: May 9, 2019
 *      Author: ManuelVr
 *
 */


#include "MIDI_Task.h"

#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_midi_core.h"
#include "usbd_midi_user.h"

#include "MIDI_queue.h"
#include "MIDI_Msg.h"
#include "MIDI_Usb.h"



/** USB device core handle. */
extern USB_OTG_CORE_HANDLE USB_OTG_dev;



/********************************************************
 * 				LOCAL METHODS
 *********************************************************/




/*********************************************/
/********    MIDI   Task    ******************/
/*********************************************/
void MIDI_Task(void)
{
	if ((USB_OTG_dev.dev.device_status == USB_OTG_CONFIGURED) || (USB_OTG_dev.host.ConnSts))
	{
		MIDI_ProcessUSBRxMessage();
	}


	if (USB_OTG_dev.dev.device_status == USB_OTG_CONFIGURED)
	{
		__disable_irq( );
		Handle_USBAsynchXfer(&USB_OTG_dev);
		__enable_irq( );
	}
	else if (USB_OTG_dev.host.ConnSts)
	{

	}
	else
	{
		MIDI_queue_init();
		MIDI_USBFlush();
	}
}

