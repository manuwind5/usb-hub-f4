/*
 * MIDI_Msg.h
 *
 *  Created on: Dec 5, 2019
 *      Author: ManuelVr
 */

#ifndef MIDI_MSG_H_
#define MIDI_MSG_H_

#ifdef __cplusplus
extern "C" {
#endif


#define MIDI_START_MSG		0xFA
#define MIDI_CONTINUE_MSG	0xFB
#define MIDI_STOP_MSG		0xFC
#define MIDI_CLOCK_MSG		0xF8

#define MIDI_STOP_CC		105
#define MIDI_PLAY_CC		106
#define MIDI_REC_CC			107

#define MIDI_ALL_NOTE_OFF	123

#define MIDI_PC_INACTIVE	-1
#define MIDI_BANK_INACTIVE	-1
#define MIDI_PC_DEFAULT		-2
#define MIDI_BANK_DEFAULT	-2

typedef enum
{
	MMC_Stop = 1,
	MMC_Play,
	MMC_Deferred_Play, // play after no longer busy
	MMC_Fast_Forward,
	MMC_Rewind,
	MMC_Record_Strobe, 	// Punch in
	MMC_Record_Exit,	// Punch out (music)
	MMC_Record_Pause,
	MMC_Pause // (pause playback)
} MIDI_MMC_commands_e;

typedef enum
{
    InvalidType           	= 0x00,    ///< For notifying errors
    NoteOff               	= 0x80,    ///< Note Off
    NoteOn                	= 0x90,    ///< Note On
    AfterTouchPoly        	= 0xA0,    ///< Polyphonic AfterTouch/key-pressure
    ControlChange         	= 0xB0,    ///< Control Change / Channel Mode
    ProgramChange         	= 0xC0,    ///< Program Change
    AfterTouchChannel     	= 0xD0,    ///< Channel (monophonic) AfterTouch/pressure
    PitchBend             	= 0xE0,    ///< Pitch Bend
    SystemExclusive       	= 0xF0,    ///< System Exclusive
    TimeCodeQuarterFrame  	= 0xF1,    ///< System Common - MIDI Time Code Quarter Frame
    SongPosition          	= 0xF2,    ///< System Common - Song Position Pointer
    SongSelect            	= 0xF3,    ///< System Common - Song Select
    TuneRequest				= 0xF6,    ///< System Common - Tune Request
    SysExEnd		  	  	= 0xF7,
    Clock                 	= 0xF8,    ///< System Real Time - Timing Clock
    Start                 	= 0xFA,    ///< System Real Time - Start
    Continue              	= 0xFB,    ///< System Real Time - Continue
    Stop                  	= 0xFC,    ///< System Real Time - Stop
    ActiveSensing         	= 0xFE,    ///< System Real Time - Active Sensing
    SystemReset           	= 0xFF,    ///< System Real Time - System Reset
} MIDI_Msg_e;

#define MIDI_CC_1		1 // mod wheel
#define MIDI_CC_2		2 // breath
#define MIDI_CC_3		3
#define MIDI_CC_11		11 // expression,

#define MIDI_CC_RESET_1		MIDI_CC_1 // mod wheel
#define MIDI_CC_RESET_2		MIDI_CC_2 // breath
#define MIDI_CC_RESET_3		MIDI_CC_3
#define MIDI_CC_RESET_4		MIDI_CC_11 // expression,

#define MIDI_CH_PRESS_NOTE		36 // CHANNEL PRESSURE DEFAULT NOTE



void MIDI_SendNoteOn(uint8_t ch, int8_t note, uint8_t vel);
void MIDI_SendNoteOff(uint8_t ch, int8_t note);
void MIDI_SendCtlChange(uint8_t ch, uint8_t num, uint8_t value);
void MIDI_SendProgramChange(uint8_t ch, uint8_t num);
void MIDI_SendPolyPressure(uint8_t ch, uint8_t key, uint8_t value);
void MIDI_SendChPressure(uint8_t ch, uint8_t value);
void MIDI_SendPitchBend(uint8_t ch, int16_t bend);

void MIDI_SendRealtimeThru(uint8_t data, uint16_t source);

void MIDI_SendStop(void);
void MIDI_AllNoteOff(void);
void MIDI_SendStart(void);
void MIDI_SendContinue(void);
void MIDI_SendClk(void);
void MIDI_SendMMC(MIDI_MMC_commands_e cmd);

void BPM_UsbStartMsgs(void);
void BPM_UsbContinueMsgs(void);
void BPM_UsbStopMsgs(void);
void BPM_UsbRecMsgs(void);

#ifdef __cplusplus
}
#endif

#endif /* MIDI_MSG_H_ */
