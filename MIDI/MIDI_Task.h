/*
 * PriorityTask.h
 *
 *  Created on: May 9, 2019
 *      Author: ManuelVr
 */

#ifndef MIDI_TASK_H_
#define MIDI_TASK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"


void MIDI_RealtimeMessage(uint16_t byte, uint16_t source);

uint16_t MIDI_GetClockCount(void);

void MIDI_StartUI(void);
void MIDI_StopUI(void);
void MIDI_RecUI(void);

void MIDI_Start(void);
void MIDI_Stop(void);
void MIDI_Pause(void);
void MIDI_Continue(void);
void MIDI_Reset(void);

void MIDI_ClockINT(void);
void MIDI_ClockCvINT(void);

void MIDI_Startup(void);

void MIDI_Task(void);
void MIDI_UpdateMOD(void);

void MIDI_UartSendPacket(void);

#ifdef __cplusplus
}
#endif

#endif /* MIDI_TASK_H_ */
