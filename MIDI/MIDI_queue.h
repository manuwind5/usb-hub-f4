// Queue32.h
// 4byte ring buffer (32bit version)


#ifndef MIDI_QUEUE_H__
#define MIDI_QUEUE_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif 

#define MIDI_QUEUE_SIZE (256*4)

typedef struct
{
  uint16_t top;
  uint16_t last;
  uint16_t num;
  uint32_t queue[MIDI_QUEUE_SIZE];
} MIDI_queue_s;

void MIDI_queue_init(void);
uint32_t MIDI_queue_push( MIDI_queue_s *qp, uint32_t *p);
uint32_t *MIDI_queue_pop( MIDI_queue_s *qp);


extern MIDI_queue_s midi_usb_rx_queue;

#ifdef __cplusplus
} // extern "C"
#endif

#endif
