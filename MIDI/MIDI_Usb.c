/*
 * MIDI_Usb.c
 *
 *  Created on: Jun 21, 2021
 *      Author: ManuelVr
 */
#include "MIDI_Usb.h"
//#include "USBD_MIDI_if.h"
#include "usbd_midi_user.h"

#include "MIDI_queue.h"
#include "MIDI_Task.h"
#include "MIDI_Msg.h"


void SYSEX_SendUSBSinglePackage(uint8_t * p_data, int16_t length, uint16_t last)
{
	uint8_t buffer[4];
	uint16_t data_ptr = 0;
	uint16_t i;

	memset(buffer, 0, 4);

	if (last == false) {
		buffer[0] = 0x04; // SysEx starts or continues (3 bytes)
	}
	else if (length == 3) {
		buffer[0] = 0x07; // SysEx ends with following three bytes.
	}
	else if (length == 2) {
		buffer[0] = 0x06; // Single-byte System Common Message or SysEx ends with following two bytes.
	}
	else if (length == 1) {
		buffer[0] = 0x05; // Single-byte System Common Message or SysEx ends with following single byte.
	}

	for (i = 0; (i < length) && (i < 3); ++i) {
		buffer[i + 1] = p_data[data_ptr++];
	}
	length-= i;

	//	LOG_INFO("0x%x 0x%x 0x%x 0x%x\r\n", buffer[0], buffer[1], buffer[2], buffer[3]);
	MIDI_USBDataTx(buffer,4);
}

void SYSEX_SendUSB(uint8_t * p_data, int16_t length)
{
	uint8_t buffer[4];
	uint16_t data_ptr = 0;
	uint16_t i;
	uint16_t data_counter = 0;

	while (length > 0) {
		memset(buffer, 0, 4);

		if (length > 3) {
			buffer[0] = 0x04; // SysEx starts or continues (3 bytes)
		}
		else if (length == 3) {
			buffer[0] = 0x07; // SysEx ends with following three bytes.
		}
		else if (length == 2) {
			buffer[0] = 0x06; // Single-byte System Common Message or SysEx ends with following two bytes.
		}
		else if (length == 1) {
			buffer[0] = 0x05; // Single-byte System Common Message or SysEx ends with following single byte.
		}

		for (i = 0; (i < length) && (i < 3); ++i) {
			buffer[i + 1] = p_data[data_ptr++];
		}
		length-= i;

		//		LOG_INFO("0x%x 0x%x 0x%x 0x%x\r\n", buffer[0], buffer[1], buffer[2], buffer[3]);

		MIDI_USBDataTx(buffer,4);
		data_counter += 4;

		if (data_counter == USB_TX_DATA_SIZE) {
			HAL_Delay(1);
		}
	}
}

void MIDI_ProcessUSBRxMessage(void)
{
	uint8_t * p_data;
	uint16_t sysex_msg_length = 0;
	static uint8_t waiting_sysex = 0;
	// Rx
	while (midi_usb_rx_queue.num > 0)
	{
		__disable_irq();
		p_data = (uint8_t *)MIDI_queue_pop(&midi_usb_rx_queue);
		__enable_irq();
		if (p_data == 0) return;

		uint8_t data[4];
		memcpy(data, p_data, 4);

		uint8_t code_index = data[0] & 0xf;
		uint16_t cable_num = data[0] & 0xf0;
		uint8_t channel = (data[1] & 0xf) + cable_num;

		switch (code_index) {
		case 0x0: // reserved, ignore
		case 0x1: // cable events, ignore
			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x2: // Two-byte System Common messages like MTC, SongSelect, etc.
			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x3: // Three-byte System Common messages like SPP, etc.
			if (waiting_sysex == 1) waiting_sysex = 0;

			break;

			/*** SYSEX MSGS ***/
		case 0x4: // SysEx starts or continues (3 bytes)
//			if (cable_num == 0)
			{
				sysex_msg_length = 3;
				waiting_sysex = 1;
			}
			break;
		case 0x5:   // Single-byte System Common Message or SysEx ends with following single byte.
//			if (cable_num == 0)
			{
				sysex_msg_length = 1;
				waiting_sysex = 0;
			}
			break;
		case 0x6:   // SysEx ends with following two bytes.
//			if (cable_num == 0)
			{
				sysex_msg_length = 2;
				waiting_sysex = 0;
			}
			break;
		case 0x7:  // SysEx ends with following three bytes.
//			if (cable_num == 0)
			{
				sysex_msg_length = 3;
				waiting_sysex = 0;
			}
			break;
		case 0x08: // Note off

			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x09: // Note on

			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x0a://Polyphonic key-pressure
			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x0b: //ControlChange
			if (waiting_sysex == 1) waiting_sysex = 0;
			break;
		case 0x0c: //Program Change
			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x0d: //Channel Pressure
			if (waiting_sysex == 1) waiting_sysex = 0;

			break;
		case 0x0e: //PitchBend
		{

			if (waiting_sysex == 1) waiting_sysex = 0;

		}
		break;
		case 0x0f: //Realtime msgs
		{
			if (waiting_sysex == 1){
				sysex_msg_length = 1;
			}
			else if (cable_num == 0) {
			}
		}
		break;
		default:
			break;
		}
	}
}
