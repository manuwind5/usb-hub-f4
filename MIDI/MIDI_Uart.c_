/********************************************************

         @@@@@@@@@@           @@@@@@@@@@@@@@@@@@@@@@
     @@@@          @@@
   @@@               @@@      @@@                @@@
  @@                   @@       @@@            @@@
 @@@                    @@         @@@      @@@
 @@          @@         @@            @@  @@
 @@@                    @@             @@@@
  @@                   @@            @@@  @@@
   @@@               @@@          @@@        @@@
     @@@@          @@@         @@@             @@@
         @@@@@@@@@@           @@                 @@@

 *********************************************************/
/*
 * MIDI_Uart.c
 *
 *  Created on: Jan 28, 2020
 *      Author: ManuelVr
 */

#include "main.h"
#include "MIDI_Task.h"
#include "MIDI_Msg.h"
#include "MIDI_Uart.h"
#include "REC.h"
#include "CV.h"
#include "UI.h"
#include "CONFIG.h"
#include "SYSEX.h"
#include "SYSTEM.h"
#include "MIDI_Keyboard.h"

#define MIDI_UART_DATA_SIZE (128U)
#define MIDI_UART_DATA_SIZE_TX (256U)
#define MIDI_SENDDATA_MAX	32

#define MIDI_OUT_JACK_NUM (1)
#define MIDI_IN_JACK_NUM MIDI_UART_SOURCE_SIZE

typedef enum{
	START_ANALYSIS,    // Initial Status, including exception.
	WAIT_DATA1,        // Waiting data byte(1st byte)
	WAIT_DATA2,        // Waiting data byte(2nd byte)
	WAIT_SYSTEM_DATA,  // Waiting data byte(system exclusive)
	END_ANALYSIS       // Analysis is ended.
}AnalysisStatus;

typedef enum{
	MSG_NOTHING,    // Exception(can't resolved, missing data, etc.)
	MSG_SYSEX,      // System Exclusive message
	MSG_ONE_BYTE,
	MSG_TWO_BYTE,
	MSG_THREE_BYTE,
}EventType;

typedef struct{
	AnalysisStatus stat;
	EventType type;
	bool is_system_common;
	uint8_t data_idx;
}MidiAnalysisStatus;

typedef struct{
	uint8_t length;
	uint8_t midi_byte[MIDI_SENDDATA_MAX]; //data_byte[0]=MSB, [1]=LSB, [2]=OTHER...(e.g. sysEx, Control Change...)
}MIDIEvent;

static uint8_t midi_uart_tx_buffer[MIDI_IN_JACK_NUM][MIDI_UART_DATA_SIZE_TX];
static uint8_t midi_uart_rx_buffer[MIDI_IN_JACK_NUM][MIDI_UART_DATA_SIZE];

static __IO uint16_t midi_uart_tx_ptr_in[MIDI_IN_JACK_NUM]  = {0};
static __IO uint16_t midi_uart_tx_ptr_out[MIDI_IN_JACK_NUM] = {0};
static __IO uint16_t midi_uart_tx_length[MIDI_IN_JACK_NUM]  = {0};

static __IO uint16_t midi_uart_rx_ptr_in[MIDI_IN_JACK_NUM]  = {0};
static __IO uint16_t midi_uart_rx_ptr_out[MIDI_IN_JACK_NUM] = {0};
static __IO uint16_t midi_uart_rx_process_byte[MIDI_IN_JACK_NUM] = {0};
static __IO uint8_t rx_byte[MIDI_IN_JACK_NUM];

__IO int16_t midi_uart_tx_capacity[MIDI_IN_JACK_NUM]  = {MIDI_UART_DATA_SIZE_TX, MIDI_UART_DATA_SIZE_TX};
__IO uint8_t midi_uart_tx_busy[MIDI_IN_JACK_NUM] = {0};

static MidiAnalysisStatus analyzed_status[MIDI_IN_JACK_NUM];
static MIDIEvent midi_event[MIDI_IN_JACK_NUM] = {0};	//received midi data

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;

static UART_HandleTypeDef * const uart_tx_handler[MIDI_IN_JACK_NUM] = {&huart1, &huart3};
//static const UART_HandleTypeDef * uart_rx_handler[MIDI_IN_JACK_NUM] = {&huart2, &huart3};

const uint8_t midi_sysex_source[MIDI_IN_JACK_NUM] = {MIDI_SOURCE_TRS, MIDI_SOURCE_BLE};

/******************/
/* LOCAL METHODS */
/******************/

static void MidiSplitEnable(void)
{
	HAL_UART_Abort(&huart1);
	__HAL_UART_DISABLE(&huart1);

	huart1.Init.BaudRate = 115200;
	/* Set the UART Communication parameters */
	uint32_t pclk;
	pclk = HAL_RCC_GetPCLK2Freq();
	huart1.Instance->BRR = UART_BRR_SAMPLING16(pclk, huart1.Init.BaudRate);

	/* In asynchronous mode, the following bits must be kept cleared:
	     - LINEN and CLKEN bits in the USART_CR2 register,
	     - SCEN, HDSEL and IREN  bits in the USART_CR3 register.*/
	CLEAR_BIT(huart1.Instance->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
	CLEAR_BIT(huart1.Instance->CR3, (USART_CR3_SCEN | USART_CR3_HDSEL | USART_CR3_IREN));

	/* Enable the peripheral */
	__HAL_UART_ENABLE(&huart1);
}

static void MidiSplitDisable(void)
{
	HAL_UART_Abort(&huart1);

	__HAL_UART_DISABLE(&huart1);

	huart1.Init.BaudRate = 31250;
	/* Set the UART Communication parameters */
	uint32_t pclk;
	pclk = HAL_RCC_GetPCLK2Freq();
	huart1.Instance->BRR = UART_BRR_SAMPLING16(pclk, huart1.Init.BaudRate);
	/* In asynchronous mode, the following bits must be kept cleared:
		     - LINEN and CLKEN bits in the USART_CR2 register,
		     - SCEN, HDSEL and IREN  bits in the USART_CR3 register.*/
	CLEAR_BIT(huart1.Instance->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
	CLEAR_BIT(huart1.Instance->CR3, (USART_CR3_SCEN | USART_CR3_HDSEL | USART_CR3_IREN));

	/* Enable the peripheral */
	__HAL_UART_ENABLE(&huart1);
}

static MIDI_Output_e MidiUartCheckThru(uint8_t cable_num)
{
	MIDI_Output_e midi_thru = MIDI_OUTPUT_OFF;

	if ((cable_num == MIDI_UART_SOURCE_TRS) &&
			(config_list[conf_midi_trs_thru] == CONFIG_MIDI_THRU_ON))
	{
		midi_thru = MIDI_OUTPUT_ALL;
	}
	else if ((cable_num == MIDI_UART_SOURCE_BLE) &&
			(config_list[conf_midi_ble_thru] == CONFIG_MIDI_THRU_ON))
	{
		midi_thru = MIDI_OUTPUT_ALL;
	}
	return midi_thru;
}


static uint16_t MidiUartAnalyzeByte(uint8_t cable_num)
{
	uint8_t upper_half_byte= (rx_byte[cable_num]) & 0xF0;

	if( upper_half_byte & 0x80 ){//0x80-0xFF:status byte

		switch(upper_half_byte){

		case 0xF0://0xF0-0xFF:system message
			switch(rx_byte[cable_num])
			{
			case 0xF0://SysEx Start
				analyzed_status[cable_num].data_idx = 0;
				midi_event[cable_num].midi_byte[ analyzed_status[cable_num].data_idx++ ] = rx_byte[cable_num];
				analyzed_status[cable_num].type = MSG_SYSEX;
				analyzed_status[cable_num].stat = WAIT_SYSTEM_DATA;
				break;

			case 0xF7://SysEx End
				midi_event[cable_num].midi_byte[ analyzed_status[cable_num].data_idx++ ] = rx_byte[cable_num];
				midi_event[cable_num].length = analyzed_status[cable_num].data_idx;
				analyzed_status[cable_num].stat = END_ANALYSIS;
				break;

			case 0xF2://Song Position
				midi_event[cable_num].midi_byte[0] = rx_byte[cable_num];
				analyzed_status[cable_num].type = MSG_THREE_BYTE;
				analyzed_status[cable_num].stat = WAIT_DATA1;
				break;

			case 0xF1://Time Code
			case 0xF3://Song Select
				midi_event[cable_num].midi_byte[0] = rx_byte[cable_num];
				analyzed_status[cable_num].type = MSG_TWO_BYTE;
				analyzed_status[cable_num].stat = WAIT_DATA1;
				break;

			case 0xF4://Undefined
			case 0xF5://Undefined
			case 0xF6://Tune request
			case 0xF8://Timing clock
			case 0xF9://Undefined
			case 0xFA://Start
			case 0xFB://Continue
			case 0xFC://Stop
			case 0xFD://Undefined
			case 0xFE://Active Sensing
			case 0xFF://Reset
				midi_event[cable_num].midi_byte[0] = rx_byte[cable_num];
				midi_event[cable_num].length = 1;
				analyzed_status[cable_num].type = MSG_ONE_BYTE;
				analyzed_status[cable_num].stat = END_ANALYSIS;
				break;
			}
			analyzed_status[cable_num].is_system_common = true;
			break;

			case 0x80://Note Off
			case 0x90://Note On
			case 0xA0://Polyphonic key-pressure
			case 0xB0://ControlChange
			case 0xE0://PitchBend
				midi_event[cable_num].midi_byte[0] = rx_byte[cable_num];
				analyzed_status[cable_num].type = MSG_THREE_BYTE;
				analyzed_status[cable_num].stat = WAIT_DATA1;
				analyzed_status[cable_num].is_system_common = false;
				break;

			case 0xC0://Program Change
			case 0xD0://Channel pressure
				midi_event[cable_num].midi_byte[0] = rx_byte[cable_num];
				analyzed_status[cable_num].type = MSG_TWO_BYTE;
				analyzed_status[cable_num].stat = WAIT_DATA1;
				analyzed_status[cable_num].is_system_common = false;
				break;

			default:
				analyzed_status[cable_num].type = MSG_NOTHING;
				analyzed_status[cable_num].stat = START_ANALYSIS;
				analyzed_status[cable_num].is_system_common = false;
				break;
		}
	} else {//0x00-0x7F:data byte

		switch(analyzed_status[cable_num].stat){

		case WAIT_DATA1:
			midi_event[cable_num].midi_byte[1] = rx_byte[cable_num];

			if(MSG_THREE_BYTE == analyzed_status[cable_num].type ){
				analyzed_status[cable_num].stat = WAIT_DATA2;
			}else if( MSG_TWO_BYTE == analyzed_status[cable_num].type ){
				midi_event[cable_num].length = 2;
				analyzed_status[cable_num].stat = END_ANALYSIS;
			}else{
				analyzed_status[cable_num].stat = START_ANALYSIS;
			}
			break;

		case WAIT_DATA2:
			if(MSG_THREE_BYTE == analyzed_status[cable_num].type ){
				midi_event[cable_num].midi_byte[2] = rx_byte[cable_num];
				midi_event[cable_num].length = 3;
				analyzed_status[cable_num].stat = END_ANALYSIS;
			}else{
				analyzed_status[cable_num].stat = START_ANALYSIS;
			}
			break;

		case WAIT_SYSTEM_DATA:
			midi_event[cable_num].midi_byte[ analyzed_status[cable_num].data_idx++ ] = rx_byte[cable_num];

			if(analyzed_status[cable_num].data_idx > (MIDI_SENDDATA_MAX - 1) ){
				analyzed_status[cable_num].stat = END_ANALYSIS;
			}
			break;

		case END_ANALYSIS://running status:When status byte is omitted.
			midi_event[cable_num].midi_byte[1] = rx_byte[cable_num];
			if(MSG_THREE_BYTE == analyzed_status[cable_num].type){
				analyzed_status[cable_num].stat = WAIT_DATA2;
			}else if(MSG_TWO_BYTE == analyzed_status[cable_num].type){
				midi_event[cable_num].length = 2;
				analyzed_status[cable_num].stat = END_ANALYSIS;
			}
			break;

		case START_ANALYSIS:
			break;

		default:
			break;
		}
	}

	if(END_ANALYSIS == analyzed_status[cable_num].stat){
		return true;
	}else{
		return false;
	}
}

static void MidiUartProcessMsg(uint8_t cable_num)
{
	uint8_t upper_half_byte= (midi_event[cable_num].midi_byte[0]) & 0xF0;
	uint8_t channel = midi_event[cable_num].midi_byte[0] & 0xF;
	switch(upper_half_byte){

	case 0xF0://0xF0-0xFF:system message
		switch(midi_event[cable_num].midi_byte[0])
		{

		case 0xF0://SysEx Start
			SYSEX_Process(midi_event[cable_num].midi_byte, midi_event[cable_num].length, midi_sysex_source[cable_num]);
			break;

		case 0xF7://SysEx End
			//			SYSEX_Process(midi_event[cable_num].midi_byte, midi_event[cable_num].length, midi_sysex_source[cable_num]);
			break;

		case 0xF2://Song Position
			break;

		case 0xF1://Time Code
		case 0xF3://Song Select
			break;

		case 0xF4://Undefined
		case 0xF5://Undefined
		case 0xF6://Tune request
		case 0xF9://Undefined
			break;

		case 0xF8://Timing clock
		case 0xFA://Start
		case 0xFB://Continue
		case 0xFC://Stop
			//			if (BPM_GetSync() == BPM_SYNC_MIDI)
		{
			MIDI_RealtimeMessage(midi_event[cable_num].midi_byte[0], BPM_SYNC_MIDI);
		}

		if ((cable_num == MIDI_UART_SOURCE_TRS) && (config_list[conf_midi_trs_thru] == CONFIG_MIDI_THRU_ON))
		{
			MIDI_SendRealtimeThru(midi_event[cable_num].midi_byte[0], MIDI_SOURCE_TRS);
		}
		else if ((cable_num == MIDI_UART_SOURCE_BLE) && (config_list[conf_midi_ble_thru] == CONFIG_MIDI_THRU_ON))
		{
			MIDI_SendRealtimeThru(midi_event[cable_num].midi_byte[0], MIDI_SOURCE_BLE);
		}

		break;
		case 0xFD://Undefined
		case 0xFE://Active Sensing
		case 0xFF://Reset
			break;
		}
		break;

		case NoteOff://Note Off
			if (UI_GetMode() == UI_MODE_CALIB_FACTORY) {

			} else {
				MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

				if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
					SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
					MIDI_KeyboardNoteOff(p_seq_selected, midi_event[cable_num].midi_byte[1], midi_thru);
				} else {
					MIDI_KeyboardNoteOffChannelFilter(channel, midi_event[cable_num].midi_byte[1], midi_thru);
				}

				if (midi_thru != MIDI_OUTPUT_OFF) {
					MIDI_SendNoteOff(0x30|channel, midi_event[cable_num].midi_byte[1], MIDI_OUTPUT_ALL);
				}
			}
			break;
		case NoteOn://Note On
			if ((UI_GetMode() == UI_MODE_CALIB_FACTORY)) {
				CV_UserCalibration(channel, midi_event[cable_num].midi_byte[1]);
			}
			else
			{
				MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

				uint8_t vel = midi_event[cable_num].midi_byte[2];

				if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
					SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
					// Interpret as NOTE OFF
					if (vel == 0) MIDI_KeyboardNoteOff(p_seq_selected, midi_event[cable_num].midi_byte[1], midi_thru);
					else MIDI_KeyboardNoteOn(p_seq_selected, midi_event[cable_num].midi_byte[1], vel, 0xFF, midi_thru, 0);
				} else {
					// Interpret as NOTE OFF
					if (vel == 0) MIDI_KeyboardNoteOffChannelFilter(channel, midi_event[cable_num].midi_byte[1], midi_thru);
					else MIDI_KeyboardNoteOnChannelFilter(channel, midi_event[cable_num].midi_byte[1], vel, midi_thru);
				}

				// OXI Split
				if (midi_thru != MIDI_OUTPUT_OFF) {
					MIDI_SendNoteOn(0x30|channel, midi_event[cable_num].midi_byte[1], vel, MIDI_OUTPUT_ALL);
				}
			}
			break;
		case 0xA0://Polyphonic key-pressure
		{
			MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

			if (midi_thru != MIDI_OUTPUT_OFF) {
				if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
					SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
					MIDI_SendPolyPressure(p_seq_selected->midi_channel, midi_event[cable_num].midi_byte[1], midi_event[cable_num].midi_byte[2], midi_thru);
				} else {
					MIDI_SendPolyPressure(channel, midi_event[cable_num].midi_byte[1], midi_event[cable_num].midi_byte[2], midi_thru);
				}

				// OXI Split
				MIDI_SendPolyPressure(0x30|channel, midi_event[cable_num].midi_byte[1], midi_event[cable_num].midi_byte[2], MIDI_OUTPUT_TRS);
			}
			break;
		}
		case 0xB0://ControlChange
		{
			MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

			if (midi_thru != MIDI_OUTPUT_OFF) {
				if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
					SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
					MIDI_SendCtlChange(p_seq_selected->midi_channel, midi_event[cable_num].midi_byte[1], midi_event[cable_num].midi_byte[2], midi_thru);
				} else {
					MIDI_SendCtlChange(channel, midi_event[cable_num].midi_byte[1], midi_event[cable_num].midi_byte[2], midi_thru);
				}

				// OXI Split
				MIDI_SendCtlChange(0x30|channel, midi_event[cable_num].midi_byte[1], midi_event[cable_num].midi_byte[2], MIDI_OUTPUT_TRS);
			}
			break;
		}
		case 0xC0://Program Change
		{
			MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

			if (midi_thru != MIDI_OUTPUT_OFF) {
				if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
					SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
					MIDI_SendProgramChange(p_seq_selected->midi_channel, midi_event[cable_num].midi_byte[1], midi_thru);
				} else {
					MIDI_SendProgramChange(channel, midi_event[cable_num].midi_byte[1], midi_thru);
				}

				// OXI Split
				MIDI_SendProgramChange(0x30|channel, midi_event[cable_num].midi_byte[1], MIDI_OUTPUT_TRS);
			}
			break;
		}
		case 0xD0://Channel pressure
		{
			MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

			if (midi_thru != MIDI_OUTPUT_OFF) {
				if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
					SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
					MIDI_SendChPressure(p_seq_selected->midi_channel, midi_event[cable_num].midi_byte[1], midi_thru);
				} else {
					MIDI_SendChPressure(channel, midi_event[cable_num].midi_byte[1], midi_thru);
				}

				// OXI Split
				MIDI_SendChPressure(0x30|channel, midi_event[cable_num].midi_byte[1], MIDI_OUTPUT_TRS);
			}
			break;
		}
		case 0xE0://PitchBend
		{
			MIDI_Output_e midi_thru = MidiUartCheckThru(cable_num);

			int16_t pitch_bend = midi_event[cable_num].midi_byte[1] + (midi_event[cable_num].midi_byte[2] << 7);

			if (config_list[conf_midi_channel_filter] == CONFIG_MIDI_CHANNEL_FILTER_OFF) {
				SEQ_s * p_seq_selected = SEQ_GetSelectedSequence();
				p_seq_selected->pitch_bend = pitch_bend;

				// TODO record automation...

				// Touch CVs
				CV_Touch(p_seq_selected, 0xFF); // TODO multitrack last track
				if (midi_thru != MIDI_OUTPUT_OFF) {
					MIDI_SendPitchBend(p_seq_selected->midi_channel, pitch_bend, midi_thru);
				}
			} else {
				//	MIDI_KeyboardPitchBendChannelFilter(channel, pitch_bend);
				if (midi_thru != MIDI_OUTPUT_OFF) {
					MIDI_SendPitchBend(channel, pitch_bend, midi_thru);
				}
			}

			// OXI Split
			if (midi_thru != MIDI_OUTPUT_OFF)
			{
				MIDI_SendPitchBend(0x30|channel, pitch_bend, MIDI_OUTPUT_TRS);
			}
		}
		break;
		default:
			break;
	}
}


/******************************************/
/*********** EXPORTED METHODS *************/
/******************************************/

void MIDI_UartTRSTx(uint8_t *msg, uint16_t length)
{
	uint16_t i = 0;
	__disable_irq();
	if (midi_uart_tx_capacity[MIDI_UART_SOURCE_TRS] < length) return;

	/* Buffer push */
	while (i < length)
	{
		midi_uart_tx_buffer[MIDI_UART_SOURCE_TRS][midi_uart_tx_ptr_in[MIDI_UART_SOURCE_TRS]] = *(msg + i);
		midi_uart_tx_ptr_in[MIDI_UART_SOURCE_TRS]++;
		midi_uart_tx_capacity[MIDI_UART_SOURCE_TRS]--;

		i++;
		if (midi_uart_tx_ptr_in[MIDI_UART_SOURCE_TRS] == MIDI_UART_DATA_SIZE_TX)
		{
			midi_uart_tx_ptr_in[MIDI_UART_SOURCE_TRS] = 0;
		}
		if (midi_uart_tx_ptr_in[MIDI_UART_SOURCE_TRS] == midi_uart_tx_ptr_out[MIDI_UART_SOURCE_TRS])
		{
			Error_Handler(err_midi_uart);
		}
	}
	__enable_irq();
}

void MIDI_UartBLETx(uint8_t *msg, uint16_t length)
{
	uint16_t i = 0;
	__disable_irq();
	if (midi_uart_tx_capacity[MIDI_UART_SOURCE_BLE] < length) return;

	// BLE FLAG tx
	if (SYSTEM_GetBLEStatus() == SYS_BLE_CONNECTED) {
		HAL_GPIO_WritePin(BLE_BOOT_GPIO_Port, BLE_BOOT_Pin, GPIO_PIN_SET);
	}

	/* Buffer push */
	while (i < length)
	{
		midi_uart_tx_buffer[MIDI_UART_SOURCE_BLE][midi_uart_tx_ptr_in[MIDI_UART_SOURCE_BLE]] = *(msg + i);
		midi_uart_tx_ptr_in[MIDI_UART_SOURCE_BLE]++;
		midi_uart_tx_capacity[MIDI_UART_SOURCE_BLE]--;

		i++;
		if (midi_uart_tx_ptr_in[MIDI_UART_SOURCE_BLE] == MIDI_UART_DATA_SIZE_TX)
		{
			midi_uart_tx_ptr_in[MIDI_UART_SOURCE_BLE] = 0;
		}
		if (midi_uart_tx_ptr_in[MIDI_UART_SOURCE_BLE] == midi_uart_tx_ptr_out[MIDI_UART_SOURCE_BLE])
		{
			Error_Handler(err_midi_uart);
		}
	}
	__enable_irq();
}

// TODO: separate BLE and SPLIT UART
void MIDI_UartProcessByte(void)
{
	for (uint16_t cable_num = 0; cable_num < MIDI_IN_JACK_NUM; cable_num++)
	{
		while (midi_uart_rx_process_byte[cable_num] > 0)
		{
			midi_uart_rx_process_byte[cable_num]--;
			/* TODO: Add code here and process UART MIDI MSGs */
			rx_byte[cable_num] = midi_uart_rx_buffer[cable_num][midi_uart_rx_ptr_out[cable_num]];
			if (MidiUartAnalyzeByte(cable_num) == true)
			{
				MidiUartProcessMsg(cable_num);
			}
			midi_uart_rx_ptr_out[cable_num]++;
			if (midi_uart_rx_ptr_out[cable_num] == MIDI_UART_DATA_SIZE)
			{
				midi_uart_rx_ptr_out[cable_num] = 0;
			}
		}
	}
}

uint16_t MIDI_UartProcessByteFACTORY(uint8_t * byte)
{

	while (midi_uart_rx_process_byte[MIDI_UART_SOURCE_BLE] > 0)
	{
		midi_uart_rx_process_byte[MIDI_UART_SOURCE_BLE]--;
		rx_byte[MIDI_UART_SOURCE_BLE] = midi_uart_rx_buffer[MIDI_UART_SOURCE_BLE][midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE]];
		if (MidiUartAnalyzeByte(MIDI_UART_SOURCE_BLE) == true)
		{
			MidiUartProcessMsg(MIDI_UART_SOURCE_BLE);
		}
		midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE]++;
		if (midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE] == MIDI_UART_DATA_SIZE)
		{
			midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE] = 0;
		}
	}


	__disable_irq();
	if (midi_uart_rx_process_byte[MIDI_UART_SOURCE_TRS] > 0)
	{
		*byte = midi_uart_rx_buffer[MIDI_UART_SOURCE_TRS][midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS]];
		midi_uart_rx_process_byte[MIDI_UART_SOURCE_TRS]--;

		midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS]++;
		if (midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS] == MIDI_UART_DATA_SIZE)
		{
			midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS] = 0;
		}
		__enable_irq();
		return 1;
	}
	__enable_irq();
	return 0;
}


//void MIDI_UartTRSGetBytes(uint8_t * data, uint16_t * len)
//{
//	while (midi_uart_rx_process_byte[MIDI_UART_SOURCE_BLE] > 0)
//	{
//		midi_uart_rx_process_byte[MIDI_UART_SOURCE_BLE]--;
//		/* TODO: Add code here and process UART MIDI MSGs */
//		rx_byte[MIDI_UART_SOURCE_BLE] = midi_uart_rx_buffer[MIDI_UART_SOURCE_BLE][midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE]];
//		if (MidiUartAnalyzeByte(MIDI_UART_SOURCE_BLE) == true)
//		{
//			MidiUartProcessMsg(MIDI_UART_SOURCE_BLE);
//		}
//		midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE]++;
//		if (midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE] == MIDI_UART_DATA_SIZE)
//		{
//			midi_uart_rx_ptr_out[MIDI_UART_SOURCE_BLE] = 0;
//		}
//	}
//
//
//	data = &midi_uart_rx_buffer[MIDI_UART_SOURCE_TRS][MIDI_UART_SOURCE_TRS];
//	uint16_t temp;
//	// If buffer overflow
//	if ((midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS] + midi_uart_rx_process_byte[MIDI_UART_SOURCE_TRS]) > MIDI_UART_DATA_SIZE) {
//		temp = MIDI_UART_DATA_SIZE - midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS];
//	} else {
//		temp = midi_uart_rx_ptr_out[MIDI_UART_SOURCE_TRS];
//	}
//	*len = temp;
//}


void MIDI_UartSendPacket(void)
{
	uint16_t midi_uart_tx_ptr;
	for (uint16_t cable_num = 0; cable_num < MIDI_IN_JACK_NUM; cable_num++)
	{
		if ((midi_uart_tx_busy[cable_num] == 0) && (uart_tx_handler[cable_num]->gState == HAL_UART_STATE_READY))//(HAL_UART_GetState(&huart1) == HAL_UART_STATE_READY))
		{
			if (midi_uart_tx_ptr_out[cable_num] == MIDI_UART_DATA_SIZE_TX)
			{
				midi_uart_tx_ptr_out[cable_num] = 0;
			}

			if(midi_uart_tx_ptr_out[cable_num] == midi_uart_tx_ptr_in[cable_num])
			{
				midi_uart_tx_busy[cable_num] = 0;
				continue;
			}

#if 1
			if(midi_uart_tx_ptr_out[cable_num] > midi_uart_tx_ptr_in[cable_num])
			{
				midi_uart_tx_length[cable_num] = MIDI_UART_DATA_SIZE_TX - midi_uart_tx_ptr_out[cable_num];
			}
			else
			{
				midi_uart_tx_length[cable_num] = midi_uart_tx_ptr_in[cable_num] - midi_uart_tx_ptr_out[cable_num];
			}
#else
			midi_uart_tx_length[cable_num] = 1;
#endif

			if (midi_uart_tx_length[cable_num] > MIDI_UART_DATA_SIZE_TX)
			{
				midi_uart_tx_ptr = midi_uart_tx_ptr_out[cable_num];
				midi_uart_tx_length[cable_num] = MIDI_UART_DATA_SIZE_TX;
				midi_uart_tx_ptr_out[cable_num] += MIDI_UART_DATA_SIZE_TX;
				midi_uart_tx_length[cable_num] -= MIDI_UART_DATA_SIZE_TX;
			}
			else
			{
				midi_uart_tx_ptr = midi_uart_tx_ptr_out[cable_num];
				midi_uart_tx_ptr_out[cable_num] += midi_uart_tx_length[cable_num];
			}
			if (midi_uart_tx_length[cable_num] > 0)
			{
				midi_uart_tx_busy[cable_num] = 1;
				midi_uart_tx_capacity[cable_num] += midi_uart_tx_length[cable_num];

				//			LOG_INFO("Sending: %d\n", midi_uart_tx_length);
				//			for (uint16_t i = 0; i < midi_uart_tx_length; ++i) {
				//				LOG_INFO("0x%02X ", midi_uart_tx_buffer[midi_uart_tx_ptr + i]);
				//			}
				//			LOG_INFO("\r\n");

				// TODO Change to DMA
				HAL_UART_Transmit_IT(uart_tx_handler[cable_num], (uint8_t*)&midi_uart_tx_buffer[cable_num][midi_uart_tx_ptr], midi_uart_tx_length[cable_num]);
			}
		}
	}
}

void MIDI_UartDataRx(void)
{
	HAL_UART_Receive_IT(&huart2, midi_uart_rx_buffer[MIDI_UART_SOURCE_TRS], 1); // called once in the startup
	HAL_UART_Receive_IT(&huart3, midi_uart_rx_buffer[MIDI_UART_SOURCE_BLE], 1); // called once in the startup
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	// UART TX
	if (huart == &huart1) {
		if (midi_uart_tx_busy[MIDI_UART_SOURCE_TRS] == 1)
		{
			midi_uart_tx_busy[MIDI_UART_SOURCE_TRS] = 0;
		}
	}
	// BLE TX
	else if (huart == &huart3) {
		if (midi_uart_tx_busy[MIDI_UART_SOURCE_BLE] == 1)
		{
			midi_uart_tx_busy[MIDI_UART_SOURCE_BLE] = 0;
		}
		if (midi_uart_tx_capacity[MIDI_UART_SOURCE_BLE] == MIDI_UART_DATA_SIZE_TX)
		{
			// BLE FLAG tx END // Caution bootloader flag!!!
			HAL_GPIO_WritePin(BLE_BOOT_GPIO_Port, BLE_BOOT_Pin, GPIO_PIN_RESET);
		}
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart2) {
		midi_uart_rx_ptr_in[MIDI_UART_SOURCE_TRS]++;
		if (midi_uart_rx_ptr_in[MIDI_UART_SOURCE_TRS] == MIDI_UART_DATA_SIZE)
		{
			midi_uart_rx_ptr_in[MIDI_UART_SOURCE_TRS] = 0;
		}
		HAL_UART_Receive_IT(&huart2, &midi_uart_rx_buffer[MIDI_UART_SOURCE_TRS][midi_uart_rx_ptr_in[MIDI_UART_SOURCE_TRS]], 1);
		midi_uart_rx_process_byte[MIDI_UART_SOURCE_TRS]++;
	}
	else if (huart == &huart3) {
		midi_uart_rx_ptr_in[MIDI_UART_SOURCE_BLE]++;
		if (midi_uart_rx_ptr_in[MIDI_UART_SOURCE_BLE] == MIDI_UART_DATA_SIZE)
		{
			midi_uart_rx_ptr_in[MIDI_UART_SOURCE_BLE] = 0;
		}
		HAL_UART_Receive_IT(&huart3, &midi_uart_rx_buffer[MIDI_UART_SOURCE_BLE][midi_uart_rx_ptr_in[MIDI_UART_SOURCE_BLE]], 1);
		midi_uart_rx_process_byte[MIDI_UART_SOURCE_BLE]++;
	}


}

void MIDI_SplitDisable(void)
{
	MidiSplitDisable();
}

void MIDI_SplitEnable(void)
{
	MidiSplitEnable();

	uint8_t msg[4];
	memset(msg, 0xff, 4);

	MIDI_UartTRSTx(msg, 4);
	MIDI_UartTRSTx(msg, 4);
}

void MIDI_SplitCheck(void)
{
	if (config_list[conf_split_enabled] == CONFIG_OXI_SPLIT_OFF)
	{
		MidiSplitDisable();
	}
	else if (config_list[conf_split_enabled] == CONFIG_OXI_SPLIT_ON)
	{
		MIDI_SplitEnable();
	}
}
