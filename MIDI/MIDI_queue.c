#include "main.h"
#include <MIDI_queue.h>

#ifdef __cplusplus
extern "C"{
#endif 

MIDI_queue_s midi_usb_rx_queue __attribute__ ((section (".ccmram"))); ;

void MIDI_queue_init(void){
	MIDI_queue_s *qp = &midi_usb_rx_queue;
	qp->top = 0;
	qp->last = 0;
	qp->num = 0;
}

static uint32_t MidiQueueNext(uint32_t value){
	return (value + 1) % MIDI_QUEUE_SIZE;
}

uint32_t MIDI_queue_push( MIDI_queue_s *p_queue, uint32_t *p_data){
	uint32_t *q = p_queue->queue;
	uint32_t last = p_queue->last;
	uint32_t last_next = MidiQueueNext(last);
	if(last_next == p_queue->top){
		return 0;
	}
#if 0
	if (*(uint8_t*)p_data == 0x0f)
	{
		__NOP();
	}
#endif

	*(q+last) = *p_data;
	p_queue->last = last_next;
	p_queue->num ++;
	return 1;
}

uint32_t * MIDI_queue_pop( MIDI_queue_s *p_queue){
	uint32_t *pRes = (uint32_t *)0;
	uint32_t *p = p_queue->queue;
	uint32_t top = p_queue->top;
	if(top != p_queue->last){
		pRes = p+top;
		p_queue->top = MidiQueueNext(top);
		p_queue->num --;
	}
	return pRes;
}

#ifdef __cplusplus
} // extern "C"
#endif

