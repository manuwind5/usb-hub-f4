#include "utils.h"

#include <usb_core.h>
#include <usbd_req.h>
#include <usb_regs.h>
#include <usbh_conf.h>

#include <app_usb_host_midi.h>

#include "usbd_midi_user.h"


// imported from mios32_usb.c
extern USB_OTG_CORE_HANDLE  USB_OTG_dev;
static uint32_t USBH_rx_buffer[MIOS32_USB_MIDI_DATA_OUT_SIZE/4];
static uint8_t USBH_tx_buffer[MIOS32_USB_MIDI_DATA_IN_SIZE];

extern uint8_t USB_Tx_Buffer   [USB_TX_DATA_SIZE];
extern uint16_t USB_Tx_ptr_out;
extern uint16_t USB_Tx_length_host;



#include <usbh_core.h>
//#include <usbh_conf.h>
#include <usbh_ioreq.h>
#include <usbh_stdreq.h>
#include <usbh_hcs.h>



extern USBH_HOST USB_Host;

// check USB_rx_buffer size
#if MIOS32_USB_MIDI_DATA_OUT_SIZE != USBH_MSC_MPS_SIZE
# error "MIOS32_USB_MIDI_DATA_OUT_SIZE and USBH_MSC_MPS_SIZE must be equal!"
#endif
#if MIOS32_USB_MIDI_DATA_IN_SIZE != USBH_MSC_MPS_SIZE
# error "MIOS32_USB_MIDI_DATA_IN_SIZE and USBH_MSC_MPS_SIZE must be equal!"
#endif

static uint8_t  USBH_hc_num_in;
static uint8_t  USBH_hc_num_out;
static uint8_t  USBH_BulkOutEp;
static uint8_t  USBH_BulkInEp;
static uint8_t  USBH_BulkInEpSize;
static uint8_t  USBH_tx_count;
static uint16_t USBH_BulkOutEpSize;

typedef enum {
	USBH_MIDI_IDLE,
	USBH_MIDI_RX,
	USBH_MIDI_TX,
} USBH_MIDI_transfer_state_t;

static USBH_MIDI_transfer_state_t USBH_MIDI_transfer_state;


/////////////////////////////////////////////////////////////////////////////
// Local prototypes
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// Local Variables
/////////////////////////////////////////////////////////////////////////////

#define MIOS32_USB_MIDI_RX_BUFFER_SIZE   USB_TX_DATA_SIZE / 4 //512 // packages
static volatile uint8_t tx_buffer_busy;

// transfer possible?
static uint8_t transfer_possible = 0;


/////////////////////////////////////////////////////////////////////////////
//! This function is called by the USB driver on cable connection/disconnection
//! \param[in] connected status (1 if connected)
//! \return < 0 on errors
//! \note Applications shouldn't call this function directly, instead please use \ref MIOS32_MIDI layer functions
/////////////////////////////////////////////////////////////////////////////
int32_t USB_MIDI_ChangeConnectionState(uint8_t connected)
{
	// in all cases: re-initialize USB MIDI driver
	// clear buffer counters and busy/wait signals again (e.g., so that no invalid data will be sent out)
	MIDI_USBFlush();

	if( connected ) {
		transfer_possible = 1;
		tx_buffer_busy = 0; // buffer not busy anymore

		USBH_MIDI_transfer_state = USBH_MIDI_IDLE;

	} else {

		// cable disconnected: disable transfers
		transfer_possible = 0;
		tx_buffer_busy = 1; // buffer busy
	}

	return 0; // no error
}

/////////////////////////////////////////////////////////////////////////////
//! This function returns the connection status of the USB MIDI interface
//! \param[in] cable number
//! \return 1: interface available
//! \return 0: interface not available
//! \note Applications shouldn't call this function directly, instead please use \ref MIOS32_MIDI layer functions
/////////////////////////////////////////////////////////////////////////////
int32_t MIOS32_USB_MIDI_CheckAvailable(uint8_t cable)
{

	if( cable >= MIOS32_USB_MIDI_NUM_PORTS )
		return 0;

	return transfer_possible ? 1 : 0;
}



/////////////////////////////////////////////////////////////////////////////
//! This function should be called periodically each mS to handle timeout
//! and expire counters.
//!
//! For USB MIDI it also checks for incoming/outgoing USB packages!
//!
//! Not for use in an application - this function is called from
//! USBH_Periodic_mS(), which is called by a task in the programming
//! model!
//! 
//! \return < 0 on errors
/////////////////////////////////////////////////////////////////////////////
int32_t USBH_Periodic_mS(void)
{
	if( USB_OTG_IsHostMode(&USB_OTG_dev) ) {
		// process the USB host events
		USBH_Process(&USB_OTG_dev, &USB_Host);
	}
	return 0;
}



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// USB Host Audio Class Callbacks
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

/**
 * @brief  USBH_MIDI_InterfaceInit
 *         Interface initialization for MSC class.
 * @param  pdev: Selected device
 * @param  hdev: Selected device property
 * @retval  USBH_Status :Response for USB MIDI driver intialization
 */
static USBH_Status USBH_InterfaceInit(USB_OTG_CORE_HANDLE *pdev, void *phost)
{
	USBH_HOST *pphost = phost;

	USB_MIDI_ChangeConnectionState(0);

	int i;
	for(i=0; i<pphost->device_prop.Cfg_Desc.bNumInterfaces && i < USBH_MAX_NUM_INTERFACES; ++i) {

		if( (pphost->device_prop.Itf_Desc[i].bInterfaceClass == 1) &&
				(pphost->device_prop.Itf_Desc[i].bInterfaceSubClass == 3))
			//			||
			//						pphost->device_prop.Itf_Desc[i].bInterfaceSubClass == 1) )
		{

			if( pphost->device_prop.Ep_Desc[i][0].bEndpointAddress & 0x80 ) {
				USBH_BulkInEp = (pphost->device_prop.Ep_Desc[i][0].bEndpointAddress);
				USBH_BulkInEpSize  = pphost->device_prop.Ep_Desc[i][0].wMaxPacketSize;
			} else {
				USBH_BulkOutEp = (pphost->device_prop.Ep_Desc[i][0].bEndpointAddress);
				USBH_BulkOutEpSize  = pphost->device_prop.Ep_Desc[i] [0].wMaxPacketSize;
			}

			if( pphost->device_prop.Ep_Desc[i][1].bEndpointAddress & 0x80 ) {
				USBH_BulkInEp = (pphost->device_prop.Ep_Desc[i][1].bEndpointAddress);
				USBH_BulkInEpSize  = pphost->device_prop.Ep_Desc[i][1].wMaxPacketSize;
			} else {
				USBH_BulkOutEp = (pphost->device_prop.Ep_Desc[i][1].bEndpointAddress);
				USBH_BulkOutEpSize  = pphost->device_prop.Ep_Desc[i][1].wMaxPacketSize;
			}

			USBH_hc_num_out = USBH_Alloc_Channel(pdev, USBH_BulkOutEp);
			USBH_hc_num_in = USBH_Alloc_Channel(pdev, USBH_BulkInEp);

			/* Open the new channels */
			USBH_Open_Channel(pdev,
					USBH_hc_num_out,
					pphost->device_prop.address,
					pphost->device_prop.speed,
					EP_TYPE_BULK,
					USBH_BulkOutEpSize);

			USBH_Open_Channel(pdev,
					USBH_hc_num_in,
					pphost->device_prop.address,
					pphost->device_prop.speed,
					EP_TYPE_BULK,
					USBH_BulkInEpSize);

			USB_MIDI_ChangeConnectionState(1);
			break;
		}
	}

	if( MIOS32_USB_MIDI_CheckAvailable(0) == 0) {
		pphost->usr_cb->DeviceNotSupported();
	}

	return USBH_OK;

}


/**
 * @brief  USBH_InterfaceDeInit
 *         De-Initialize interface by freeing host channels allocated to interface
 * @param  pdev: Selected device
 * @param  hdev: Selected device property
 * @retval None
 */
static void USBH_InterfaceDeInit(USB_OTG_CORE_HANDLE *pdev, void *phost)
{
	if( USBH_hc_num_out ) {
		USB_OTG_HC_Halt(pdev, USBH_hc_num_out);
		USBH_Free_Channel  (pdev, USBH_hc_num_out);
		USBH_hc_num_out = 0;     /* Reset the Channel as Free */
	}

	if( USBH_hc_num_in ) {
		USB_OTG_HC_Halt(pdev, USBH_hc_num_in);
		USBH_Free_Channel  (pdev, USBH_hc_num_in);
		USBH_hc_num_in = 0;     /* Reset the Channel as Free */
	}
}

/**
 * @brief  USBH_ClassRequest
 *         This function will only initialize the MSC state machine
 * @param  pdev: Selected device
 * @param  hdev: Selected device property
 * @retval  USBH_Status :Response for USB Set Protocol request
 */
static USBH_Status USBH_ClassRequest(USB_OTG_CORE_HANDLE *pdev, void *phost)
{
	USBH_Status status = USBH_OK;
	return status;
}

/**
 * @brief  USBH_Handle
 *         MSC state machine handler
 * @param  pdev: Selected device
 * @param  hdev: Selected device property
 * @retval USBH_Status
 */
static USBH_Status USBH_Handle(USB_OTG_CORE_HANDLE *pdev, void *phost)
{
	if( transfer_possible ) {
		USBH_HOST *pphost = phost;

		if( HCD_IsDeviceConnected(pdev) ) {

			uint8_t force_rx_req = 0;

			if( USBH_MIDI_transfer_state == USBH_MIDI_TX ) {
				URB_STATE URB_State = HCD_GetURB_State(pdev, USBH_hc_num_out);

				if( URB_State == URB_IDLE ) {
					// wait...
				} else if( URB_State == URB_DONE ) {
					USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
				} else if( URB_State == URB_STALL ) {
					// Issue Clear Feature on OUT endpoint
					if( USBH_ClrFeature(pdev, pphost, USBH_BulkOutEp, USBH_hc_num_out) == USBH_OK ) {
						USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
					}
				} else if( URB_State == URB_NOTREADY ) {
					// send again
					USBH_BulkSendData(&USB_OTG_dev, (uint8_t *)USBH_tx_buffer, USBH_tx_count, USBH_hc_num_out);
				} else if( URB_State == URB_ERROR ) {
					USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
				}
			} else if( USBH_MIDI_transfer_state == USBH_MIDI_RX ) {
				URB_STATE URB_State = HCD_GetURB_State(pdev, USBH_hc_num_in);
				if( URB_State == URB_IDLE || URB_State == URB_DONE ) {
					// data received from receive
					//uint32_t count = HCD_GetXferCnt(pdev, USBH_hc_num_in) / 4;
					// Note: HCD_GetXferCnt returns a counter which isn't zeroed immediately on a USBH_BulkReceiveData() call

					uint32_t count = USB_OTG_dev.host.hc[USBH_hc_num_in].xfer_count;

					// push data into FIFO
					if( !count ) {
						USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
					} else
					{
						// copy received packages into receive buffer
						// this operation should be atomic
						__disable_irq();
						MIDI_DataRx((uint8_t *)USBH_rx_buffer, count);
						__enable_irq();

						USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
						force_rx_req = 1;
					}
				} else if( URB_State == URB_STALL ) {
					// Issue Clear Feature on IN endpoint
					if( USBH_ClrFeature(pdev, pphost, USBH_BulkInEp, USBH_hc_num_in) == USBH_OK ) {
						USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
					}
				} else if( URB_State == URB_ERROR || URB_State == URB_NOTREADY ) {
					USBH_MIDI_transfer_state = USBH_MIDI_IDLE;
				}
			}


			if( USBH_MIDI_transfer_state == USBH_MIDI_IDLE ) {
				if( !force_rx_req && USB_Tx_length_host && transfer_possible ) {
					// atomic operation to avoid conflict with other interrupts
					__disable_irq();

					USBH_tx_count = MIN(USB_Tx_length_host, USBH_BulkOutEpSize);
					USBH_tx_count = MIN(USBH_tx_count, MIOS32_USB_MIDI_DATA_OUT_SIZE);
					// send to IN pipe
					USB_Tx_length_host -= USBH_tx_count;

					for(int i = 0; i < USBH_tx_count; ++i) {
						USBH_tx_buffer[i] = USB_Tx_Buffer[USB_Tx_ptr_out];
						if( ++USB_Tx_ptr_out >= USB_TX_DATA_SIZE )
							USB_Tx_ptr_out = 0;
					}
					USBH_BulkSendData(&USB_OTG_dev, (uint8_t *)USBH_tx_buffer, USBH_tx_count, USBH_hc_num_out);

					USBH_MIDI_transfer_state = USBH_MIDI_TX;
					__enable_irq();
				} else {
					// request data from device
					USBH_BulkReceiveData(&USB_OTG_dev, (uint8_t *)USBH_rx_buffer, USBH_BulkInEpSize, USBH_hc_num_in);
					USBH_MIDI_transfer_state = USBH_MIDI_RX;
				}
			}
		}
	}

	return USBH_OK;
}


const USBH_Class_cb_TypeDef MIOS32_MIDI_USBH_Callbacks = {
		USBH_InterfaceInit,
		USBH_InterfaceDeInit,
		USBH_ClassRequest,
		USBH_Handle
};

