// $Id: mios32_usb_midi.h 1800 2013-06-02 22:09:03Z tk $
/*
 * Header file for USB MIDI Driver
 *
 * ==========================================================================
 *
 *  Copyright (C) 2008 Thorsten Klose (tk@midibox.org)
 *  Licensed for personal non-commercial use only.
 *  All other rights reserved.
 * 
 * ==========================================================================
 */

#ifndef _MIOS32_USB_MIDI_H
#define _MIOS32_USB_MIDI_H

/////////////////////////////////////////////////////////////////////////////
// Global definitions
/////////////////////////////////////////////////////////////////////////////

// allowed numbers: 1..8
#ifndef MIOS32_USB_MIDI_NUM_PORTS
#define MIOS32_USB_MIDI_NUM_PORTS 2
#endif

// buffer size (should be at least >= MIOS32_USB_MIDI_DESC_DATA_*_SIZE/4)
#ifndef MIOS32_USB_MIDI_RX_BUFFER_SIZE
//#define MIOS32_USB_MIDI_RX_BUFFER_SIZE   64 // packages
#endif

#ifndef MIOS32_USB_MIDI_TX_BUFFER_SIZE
//#define MIOS32_USB_MIDI_TX_BUFFER_SIZE   64 // packages
#endif


// size of IN/OUT pipe
#ifndef MIOS32_USB_MIDI_DATA_IN_SIZE
#define MIOS32_USB_MIDI_DATA_IN_SIZE           64
#endif
#ifndef MIOS32_USB_MIDI_DATA_OUT_SIZE
#define MIOS32_USB_MIDI_DATA_OUT_SIZE          64
#endif


// endpoint assignments (don't change!)
#define MIOS32_USB_MIDI_DATA_OUT_EP 0x02
#define MIOS32_USB_MIDI_DATA_IN_EP  0x81

/////////////////////////////////////////////////////////////////////////////
// Prototypes
/////////////////////////////////////////////////////////////////////////////

//extern int32_t MIOS32_USB_MIDI_Init(uint32_t mode);

extern int32_t USB_MIDI_ChangeConnectionState(uint8_t connected);
//extern void MIOS32_USB_MIDI_EP1_IN_Callback(uint8_t bEP, uint8_t bEPStatus);
//extern void MIOS32_USB_MIDI_EP2_OUT_Callback(uint8_t bEP, uint8_t bEPStatus);

extern int32_t MIOS32_USB_MIDI_CheckAvailable(uint8_t cable);

//extern int32_t MIOS32_USB_MIDI_PackageSend_NonBlocking(mios32_midi_package_t package);
//extern int32_t MIOS32_USB_MIDI_PackageSend(mios32_midi_package_t package);
//extern int32_t MIOS32_USB_MIDI_PackageReceive(mios32_midi_package_t *package);

extern int32_t USBH_Periodic_mS(void);


/////////////////////////////////////////////////////////////////////////////
// Export global variables
/////////////////////////////////////////////////////////////////////////////


#endif /* _MIOS32_USB_MIDI_H */
