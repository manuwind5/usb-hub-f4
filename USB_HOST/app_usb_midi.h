// $Id: mios32_usb.h 2086 2014-10-22 18:07:02Z tk $
/*
 * Header file for USB Driver
 *
 * ==========================================================================
 *
 *  Copyright (C) 2008 Thorsten Klose (tk@midibox.org)
 *  Licensed for personal non-commercial use only.
 *  All other rights reserved.
 * 
 * ==========================================================================
 */

#ifndef _MIOS32_USB_H
#define _MIOS32_USB_H

#include "usbd_desc.h"

#include "usbd_midi_core.h"
#include "usbd_midi_user.h"

#include "app_usb_host_midi.h"

/////////////////////////////////////////////////////////////////////////////
// Global definitions
/////////////////////////////////////////////////////////////////////////////






/////////////////////////////////////////////////////////////////////////////
// Prototypes
/////////////////////////////////////////////////////////////////////////////

extern int32_t USBH_APP_Init(void);
extern int32_t USBH_APP_DeInit(void);

extern int32_t USBD_APP_Init(void);

extern int32_t MIOS32_USB_IsInitialized(void);
//extern int32_t MIOS32_USB_ForceSingleUSB(void);
//extern int32_t MIOS32_USB_ForceDeviceMode(void);


/////////////////////////////////////////////////////////////////////////////
// Export global variables
/////////////////////////////////////////////////////////////////////////////

//extern void (*pEpInt_IN[7])(void);
//extern void (*pEpInt_OUT[7])(void);

#endif /* _MIOS32_USB_H */
