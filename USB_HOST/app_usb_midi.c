// $Id: mioint32_t_usb.c 2087 2014-11-02 22:10:13Z tk $
//! \defgroup MIOS32_USB
//!
//! USB driver for MIOS32
//! 
//! Based on driver included in STM32 USB library
//! Some code copied and modified from Virtual_COM_Port demo
//! 
//! Applications shouldn't call these functions directly, instead please use \ref MIOS32_COM or \ref MIOS32_MIDI layer functions
//! 
//! \{
/* ==========================================================================
 *
 *  Copyright (C) 2008 Thorsten Klose (tk@midibox.org)
 *  Licensed for personal non-commercial use only.
 *  All other rights reserved.
 * 
 * ==========================================================================
 */

/////////////////////////////////////////////////////////////////////////////
// Include files
/////////////////////////////////////////////////////////////////////////////

//#include <mioint32_t.h>

// this module can be optionally disabled in a local mioint32_t_config.h file (included from mioint32_t.h)
#if !defined(MIOS32_DONT_USE_USB)

#include "main.h"

#include <usbd_core.h>
#include <usbd_def.h>
#include <usbd_desc.h>
#include <usbd_req.h>
#include <usbd_conf.h>
#include <usbh_core.h>
#include <usbh_conf.h>
#include <usb_otg.h>
#include <usb_dcd_int.h>
#include <usb_hcd_int.h>
#include <usb_regs.h>

#include <app_usb_midi.h>
#include "app_usb_host_midi.h"
#include "usbd_midi_core.h"

#include <string.h>

/////////////////////////////////////////////////////////////////////////////
// Local definitions
/////////////////////////////////////////////////////////////////////////////

#define DSCR_DEVICE	1	// Descriptor type: Device
#define DSCR_CONFIG	2	// Descriptor type: Configuration
#define DSCR_STRING	3	// Descriptor type: String
#define DSCR_INTRFC	4	// Descriptor type: Interface
#define DSCR_ENDPNT	5	// Descriptor type: Endpoint

#define CS_INTERFACE	0x24	// Class-specific type: Interface
#define CS_ENDPOINT	0x25	// Class-specific type: Endpoint

/////////////////////////////////////////////////////////////////////////////
// Global Variables
/////////////////////////////////////////////////////////////////////////////

__ALIGN_BEGIN USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;

__ALIGN_BEGIN USBH_HOST USB_Host __ALIGN_END;
extern const USBH_Class_cb_TypeDef MIOS32_MIDI_USBH_Callbacks; // implemented in mioint32_t_usb_midi.c

extern char product_str[];

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Descriptor Handling
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// User Host hooks for different device states
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////


/**
 * @brief  USBH_USR_Init
 *         Displays the message on LCD for host lib initialization
 * @param  None
 * @retval None
 */
static void USBH_USR_Init(void)
{
}

/**
 * @brief  USBH_USR_DeviceAttached
 *         Displays the message on LCD on device attached
 * @param  None
 * @retval None
 */
static void USBH_USR_DeviceAttached(void)
{  
}

/**
 * @brief  USBH_USR_UnrecoveredError
 * @param  None
 * @retval None
 */
static void USBH_USR_UnrecoveredError (void)
{
}

/**
 * @brief  USBH_DisconnectEvent
 *         Device disconnect event
 * @param  None
 * @retval None
 */
static void USBH_USR_DeviceDisconnected (void)
{
//  MIOS32_USB_MIDI_ChangeConnectionState(0);
}

/**
 * @brief  USBH_USR_ResetUSBDevice
 *         Reset USB Device
 * @param  None
 * @retval None
 */
static void USBH_USR_ResetDevice(void)
{
}


/**
 * @brief  USBH_USR_DeviceSpeedDetected
 *         Displays the message on LCD for device speed
 * @param  Devicespeed : Device Speed
 * @retval None
 */
static void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed)
{
	UNUSED(DeviceSpeed);
}

/**
 * @brief  USBH_USR_Device_DescAvailable
 *         Displays the message on LCD for device descriptor
 * @param  DeviceDesc : device descriptor
 * @retval None
 */
static void USBH_USR_Device_DescAvailable(void *DeviceDesc)
{
	UNUSED(DeviceDesc);
}

/**
 * @brief  USBH_USR_DeviceAddressAssigned
 *         USB device is successfully assigned the Address
 * @param  None
 * @retval None
 */
static void USBH_USR_DeviceAddressAssigned(void)
{
}

/**
 * @brief  USBH_USR_Conf_Desc
 *         Displays the message on LCD for configuration descriptor
 * @param  ConfDesc : Configuration descriptor
 * @retval None
 */
static void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc,
						 USBH_InterfaceDesc_TypeDef *itfDesc,
						 USBH_EpDesc_TypeDef *epDesc)
{
	UNUSED(cfgDesc);
	UNUSED(itfDesc);
	UNUSED(epDesc);
}

/**
 * @brief  USBH_USR_Manufacturer_String
 *         Displays the message on LCD for Manufacturer String
 * @param  ManufacturerString : Manufacturer String of Device
 * @retval None
 */
static void USBH_USR_Manufacturer_String(void *ManufacturerString)
{
#ifdef MIOS32_MIDI_USBH_DEBUG
  // Debug Output via UART0
  mioint32_t_midi_port_t prev_port = MIOS32_MIDI_DebugPortGet();
  MIOS32_MIDI_DebugPortSet(UART0);
  MIOS32_MIDI_SendDebugMessage("[USBH_USR] Manufacturer: %s", ManufacturerString);
  MIOS32_MIDI_DebugPortSet(prev_port);
#endif
  LOG_INFO("[USBH_USR] Manufacturer: %s", ManufacturerString);LOG_ENDL();
}

/**
 * @brief  USBH_USR_Product_String
 *         Displays the message on LCD for Product String
 * @param  ProductString : Product String of Device
 * @retval None
 */
static void USBH_USR_Product_String(void *ProductString)
{
#ifdef MIOS32_MIDI_USBH_DEBUG
  // Debug Output via UART0
  mioint32_t_midi_port_t prev_port = MIOS32_MIDI_DebugPortGet();
  MIOS32_MIDI_DebugPortSet(UART0);
  MIOS32_MIDI_SendDebugMessage("[USBH_USR] Product: %s", ProductString);
  MIOS32_MIDI_DebugPortSet(prev_port);
#endif

  LOG_INFO("[USBH_USR] Product: %s", ProductString);LOG_ENDL();
}

/**
 * @brief  USBH_USR_SerialNum_String
 *         Displays the message on LCD for SerialNum_String
 * @param  SerialNumString : SerialNum_String of device
 * @retval None
 */
static void USBH_USR_SerialNum_String(void *SerialNumString)
{
#ifdef MIOS32_MIDI_USBH_DEBUG
  // Debug Output via UART0
  mioint32_t_midi_port_t prev_port = MIOS32_MIDI_DebugPortGet();
  MIOS32_MIDI_DebugPortSet(UART0);
  MIOS32_MIDI_SendDebugMessage("[USBH_USR] Serial Number: %s", SerialNumString);
  MIOS32_MIDI_DebugPortSet(prev_port);
#endif
  LOG_INFO("[USBH_USR] Serial Number: %s", SerialNumString);LOG_ENDL();
} 

/**
 * @brief  EnumerationDone
 *         User response request is displayed to ask for
 *         application jump to class
 * @param  None
 * @retval None
 */
static void USBH_USR_EnumerationDone(void)
{
} 

/**
 * @brief  USBH_USR_DeviceNotSupported
 *         Device is not supported
 * @param  None
 * @retval None
 */
static void USBH_USR_DeviceNotSupported(void)
{

}  


/**
 * @brief  USBH_USR_UserInput
 *         User Action for application state entry
 * @param  None
 * @retval USBH_USR_Status : User response for key button
 */
static USBH_USR_Status USBH_USR_UserInput(void)
{
  return USBH_USR_RESP_OK;
}

/**
 * @brief  USBH_USR_OverCurrentDetected
 *         Device Overcurrent detection event
 * @param  None
 * @retval None
 */
static void USBH_USR_OverCurrentDetected (void)
{
}

/**
* @brief  USBH_USR_MSC_Application 
*         Demo application for mass storage
* @param  None
* @retval Staus
*/
static int USBH_USR_Application(void)
{
  return (0);
}

/**
 * @brief  USBH_USR_DeInit
 *         Deinit User state and associated variables
 * @param  None
 * @retval None
 */
static void USBH_USR_DeInit(void)
{
}


static const USBH_Usr_cb_TypeDef USBH_USR_Callbacks =
{
  USBH_USR_Init,
  USBH_USR_DeInit,
  USBH_USR_DeviceAttached,
  USBH_USR_ResetDevice,
  USBH_USR_DeviceDisconnected,
  USBH_USR_OverCurrentDetected,
  USBH_USR_DeviceSpeedDetected,
  USBH_USR_Device_DescAvailable,
  USBH_USR_DeviceAddressAssigned,
  USBH_USR_Configuration_DescAvailable,
  USBH_USR_Manufacturer_String,
  USBH_USR_Product_String,
  USBH_USR_SerialNum_String,
  USBH_USR_EnumerationDone,
  USBH_USR_UserInput,
  USBH_USR_Application,
  USBH_USR_DeviceNotSupported,
  USBH_USR_UnrecoveredError
};


#endif /* MIOS32_DONT_USE_USB_HOST */


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// BSP Layer
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
//! Initializes USB interface
//! \param[in] mode
//!   <UL>
//!     <LI>if 0, USB peripheral won't be initialized if this has already been done before
//!     <LI>if 1, USB peripheral re-initialisation will be forced
//!     <LI>if 2, USB peripheral re-initialisation will be forced, STM32 driver hooks won't be overwritten.<BR>
//!         This mode can be used for a local USB driver which installs it's own hooks during runtime.<BR>
//!         The application can switch back to MIOS32 drivers (e.g. MIOS32_USB_MIDI) by calling MIOS32_USB_Init(1)
//!   </UL>
//! \return < 0 if initialisation failed
//! \note Applications shouldn't call this function directly, instead please use \ref MIOS32_COM or \ref MIOS32_MIDI layer functions
/////////////////////////////////////////////////////////////////////////////
int32_t USBH_APP_Init(void)
{
	//  uint16_t usb_is_initialized = MIOS32_USB_IsInitialized();

	/* Init Host Library */
	USBH_Init(&USB_OTG_dev,
			USB_OTG_FS_CORE_ID,
			&USB_Host,
			(USBH_Class_cb_TypeDef *)&MIOS32_MIDI_USBH_Callbacks,
			(USBH_Usr_cb_TypeDef *)&USBH_USR_Callbacks);

	// change connection state to disconnected
	USB_MIDI_ChangeConnectionState(0);

	USB_OTG_SetCurrentMode(&USB_OTG_dev, HOST_MODE);

	return 0; // no error
}

int32_t USBH_APP_DeInit(void)
{
	/* Init Host Library */
	USBH_DeInit(&USB_OTG_dev, &USB_Host);

	return 0; // no error
}

int32_t USBD_APP_Init(void)
{
	USBD_Init(&USB_OTG_dev,
				USB_OTG_FS_CORE_ID,
				&USR_MIDI_desc,
				&USBD_MIDI_cb);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
//! Allows to query, if the USB interface has already been initialized.<BR>
//! This function is used by the bootloader to avoid a reconnection, it isn't
//! relevant for typical applications!
//! \return 1 if USB already initialized, 0 if not initialized
/////////////////////////////////////////////////////////////////////////////
int32_t MIOS32_USB_IsInitialized(void)
{
  // we assume that initialisation has been done when B-Session valid flag is set
  __IO USB_OTG_GREGS *GREGS = (USB_OTG_GREGS *)(USB_OTG_FS_BASE_ADDR + USB_OTG_CORE_GLOBAL_REGS_OFFSET);
  return (GREGS->GOTGCTL & (1 << 19)) ? 1 : 0;
}


//! \}

