/**
 * @file usbd_midi_user.h
 * @author Philip Karlsson
 * @version V1.0.0
 * @date 29-November-2014
 * @brief The header for the unser interface to usbd_midi_core
 */

#ifndef __USBD_MIDI_USER_H
#define __USBD_MIDI_USER_H
/* Includes ------- */

#ifdef __cplusplus
extern "C" {
#endif

//#include "stm32f4xx_hal_conf.h"

#include "usbd_midi_core.h"
#include "usbd_conf.h"

extern MIDI_IF_Prop_TypeDef MIDI_fops;

uint16_t MIDI_DataRx(uint8_t *msg, uint16_t length);
uint16_t MIDI_USBDataTx(uint8_t *msg, uint16_t length);
void MIDI_USBFlush(void);

#ifdef __cplusplus
}
#endif

#endif
