/**
 * @file usbd_midi_core.h
 * @author Philip Karlsson
 * @version V1.0.0
 * @date 29-November-2014
 * @brief This is the header for the implementation of the midi class.
 *
 */

#ifndef __USB_MIDI_CORE_H_
#define __USB_MIDI_CORE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include  "usbd_ioreq.h"
#include  "usbd_conf.h"

/* The structure of the callbacks related to the midi core driver.. */
extern const USBD_Class_cb_TypeDef  USBD_MIDI_cb;

/* User callbacks for the midi driver */
typedef struct _MIDI_IF_PROP
{
	uint16_t (*pIf_MidiRx)		(uint8_t *msg, uint16_t length);
	uint16_t (*pIf_MidiTx)		(uint8_t *msg, uint16_t length);
}
MIDI_IF_Prop_TypeDef;


extern uint8_t USB_Tx_Buffer   [USB_TX_DATA_SIZE];
extern uint16_t USB_Tx_ptr_in;
extern uint16_t USB_Tx_ptr_out;
extern uint16_t USB_Tx_length;
extern uint16_t USB_Tx_length_host;
extern uint8_t  USB_Tx_State;

void Handle_USBAsynchXfer (void *pdev);

#ifdef __cplusplus
}
#endif

#endif  // __USB_CDC_CORE_H_
