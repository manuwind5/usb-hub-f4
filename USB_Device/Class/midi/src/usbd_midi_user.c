/**
 * @file usbd_midi_user.c
 * @author Philip Karlsson
 * @version V1.0.0
 * @date 29-November-2014
 * @brief This file contains user interface to the midi class driver.
 *
 * This is the implementation of the user interface to usbd_midi_core. MIDI_DataRx 
 * is called whenever new midi data has arrived to the device. In order to send data
 * call send_MIDI_msg with the appropriate midi message to be sent to the host.
 */

/* Includes */
#include "main.h"
#include "usbd_midi_user.h"
#include "stm32f4xx_hal_conf.h"

#include "MIDI_queue.h"
/* Private functions ------------
 *
 *
 */

MIDI_IF_Prop_TypeDef MIDI_fops =
{
		MIDI_DataRx,
		MIDI_USBDataTx
};

/* This is the callback function that is called
 * whenever a midi message is revieved.
 */
uint16_t MIDI_DataRx(uint8_t *msg, uint16_t length)
{
	uint16_t cnt;
	uint16_t msgs = length / 4;
	uint16_t chk = length % 4; /* MIDI MSGS ARE 4 BYTES LONG */

	if(chk == 0)
	{
		/* TODO: check */
		for(cnt = 0;cnt < msgs;cnt ++)
		{
			/* read from USB_Rx_Buffer */
			//	__disable_irq( );
			if (MIDI_queue_push(&midi_usb_rx_queue,((uint32_t *)msg)+cnt) == 0)
			{
#if 1
//#ifdef DEBUG_USB
				// Error_Handler(err_midi_usb_rx);
				LOG_INFO("USB MIDI RX FULL");LOG_ENDL();
				return 1;
#endif
			}
			//	__enable_irq( );
		}
	}
	return 0;
}

/* This function is called in order to send a midi message
 * over USB.
 */
uint16_t MIDI_USBDataTx(uint8_t *msg, uint16_t length){
	uint32_t i = 0;
	__disable_irq( );
	while (i < length) {
		USB_Tx_Buffer[USB_Tx_ptr_in] = *(msg + i);
		USB_Tx_ptr_in++;
		i++;
		USB_Tx_length_host++;
		/* To avoid buffer overflow */
		if (USB_Tx_ptr_in == USB_TX_DATA_SIZE) {
			USB_Tx_ptr_in = 0;
		}
	}
	__enable_irq( );
	return USBD_OK;
}

void MIDI_USBFlush(void)
{
	USB_Tx_ptr_in = 0;
	USB_Tx_ptr_out = 0;
	USB_Tx_length = 0;
	USB_Tx_length_host = 0;
}
