/********************************************************

         @@@@@@@@@@           @@@@@@@@@@@@@@@@@@@@@@
     @@@@          @@@
   @@@               @@@      @@@                @@@
  @@                   @@       @@@            @@@
 @@@                    @@         @@@      @@@
 @@          @@         @@            @@  @@
 @@@                    @@             @@@@
  @@                   @@            @@@  @@@
   @@@               @@@          @@@        @@@
     @@@@          @@@         @@@             @@@
         @@@@@@@@@@           @@                 @@@

 *********************************************************/
/*
 * usbd_midi_core.c
 *
 *  Created on: Jul 8, 2021
 *      Author: ManuelVr
 */

/* Includes ------------------------------------------------------------------*/
#include "usbd_midi_core.h"
#include "usbd_desc.h"
#include "usbd_req.h"

/***********************************
 * MIDI Device library callbacks
 *
 **********************************/
static const uint8_t  *USBD_midi_GetCfgDesc (uint8_t speed, uint16_t *length); // Done

static uint8_t  usbd_midi_Init        (void  *pdev, uint8_t cfgidx);
static uint8_t  usbd_midi_DeInit      (void  *pdev, uint8_t cfgidx);
static uint8_t  usbd_midi_DataIn      (void *pdev, uint8_t epnum);
static uint8_t  usbd_midi_DataOut     (void *pdev, uint8_t epnum);
static uint8_t  usbd_midi_SOF         (void *pdev);

uint16_t USB_Tx_ptr_in  = 0;
uint16_t USB_Tx_ptr_out = 0;
uint16_t USB_Tx_length  = 0;
uint16_t USB_Tx_length_host  = 0;
uint8_t  USB_Tx_State = 0;

#define DSCR_DEVICE	1	// Descriptor type: Device
#define DSCR_CONFIG	2	// Descriptor type: Configuration
#define DSCR_STRING	3	// Descriptor type: String
#define DSCR_INTRFC	4	// Descriptor type: Interface
#define DSCR_ENDPNT	5	// Descriptor type: Endpoint

#define CS_INTERFACE	0x24	// Class-specific type: Interface
#define CS_ENDPOINT	0x25	// Class-specific type: Endpoint

#define USB_MIDI_USE_AC_INTERFACE 0
#define USB_MIDI_NUM_PORTS 2

#  define USB_MIDI_AS_INTERFACE_IX       (0x00 + USB_MIDI_USE_AC_INTERFACE)
#  define USB_MIDI_NUM_INTERFACES        1
#  define USB_MIDI_INTERFACE_NUM         0x00
#  define USB_MIDI_INTERFACE_OFFSET      1

#ifndef DATA_OUT_PACKET_SIZE
#define 	DATA_OUT_PACKET_SIZE		0x40
#define 	MIDI_DATA_MAX_PACKET_SIZE   0x40   /* Endpoint IN & OUT Packet size */
#endif

# define USB_MIDI_SIZ_CLASS_DESC         (7+USB_MIDI_NUM_PORTS*(6+6+9+9)+9+(4+USB_MIDI_NUM_PORTS)+9+(4+USB_MIDI_NUM_PORTS))
# define USB_MIDI_SIZ_CONFIG_DESC        (9+USB_MIDI_USE_AC_INTERFACE*(9+9)+USB_MIDI_SIZ_CLASS_DESC)

# define USB_MIDI_SIZ_CLASS_DESC_SINGLE_USB  (7+1*(6+6+9+9)+9+(4+1)+9+(4+1))
# define USB_MIDI_SIZ_CONFIG_DESC_SINGLE_USB (9+USB_MIDI_USE_AC_INTERFACE*(9+9)+USB_MIDI_SIZ_CLASS_DESC)

#define USB_NUM_INTERFACES              (USB_MIDI_NUM_INTERFACES)
#define USB_SIZ_CONFIG_DESC             (9 + USB_MIDI_SIZ_CONFIG_DESC)



/* USB MIDI device Configuration Descriptor */
__ALIGN_BEGIN const uint8_t usbd_midi_CfgDesc[USB_SIZ_CONFIG_DESC] __ALIGN_END =
{
		// Configuration Descriptor
		9,				// Descriptor length
		DSCR_CONFIG,			// Descriptor type
		(USB_SIZ_CONFIG_DESC) & 0xff,  // Config + End Points length (LSB)
		(USB_SIZ_CONFIG_DESC) >> 8,    // Config + End Points length (LSB)
		USB_NUM_INTERFACES,    // Number of interfaces
		0x01,				// Configuration Value
		0x00,				// Configuration string
		0x80,				// Attributes (b7 - buspwr, b6 - selfpwr, b5 - rwu)
		0xFA,				// Power requirement (div 2 ma) 500mA


		///////////////////////////////////////////////////////////////////////////
		// USB MIDI
		///////////////////////////////////////////////////////////////////////////
#if USB_MIDI_USE_AC_INTERFACE
		// AC interface descriptor follows inline:
		9,  // Size of this descriptor, in bytes
		DSCR_INTRFC, // INTERFACE descriptor
		0,  // Index of this interface
		0,  // Index of this setting
		0,  // 0 endpoints
		1,  // AUDIO
		1,  // AUDIO_CONTROL
		0,  // Unused
		0,  // String ID

		// AC Class-Specific descriptor
		9,  // Size of this descriptor, in bytes
		36,  // CS_INTERFACE
		1,  // HEADER subtype
		0x0, 0x01,  // Revision of class specification - 1.0
		9, 0,  // Total size of class specific descriptors
		1,  // Number of streaming interfaces
		1,  // MIDIStreaming interface 1 belongs to this AudioControl interface
#endif

		// Standard MS Interface Descriptor
		9,				// Descriptor length
		DSCR_INTRFC,			// Descriptor type
		USB_MIDI_AS_INTERFACE_IX, // Zero-based index of this interface
		0x00,				// Alternate setting
		0x02,				// Number of end points
		0x01,				// Interface class  (AUDIO)
		0x03,				// Interface sub class  (MIDISTREAMING)
		0x00,				// Interface sub sub class
		0x05, // USBD_IDX_PRODUCT_STR,				// Interface descriptor string index

		// Class-specific MS Interface Descriptor
		7,				// Descriptor length
		CS_INTERFACE,			// Descriptor type
		0x01,				// Zero-based index of this interface
//		0x00,				// Zero-based index of this interface
		0x00,				// revision of this class specification (LSB)
		0x01,				// revision of this class specification (MSB)
		(uint8_t)(USB_MIDI_SIZ_CLASS_DESC & 0xff), // Total size of class-specific descriptors (LSB)
		(uint8_t)(USB_MIDI_SIZ_CLASS_DESC >> 8),   // Total size of class-specific descriptors (MSB)

#if USB_MIDI_NUM_PORTS >= 1
		// MIDI IN Jack Descriptor (Embedded)
		6,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x02,				// MIDI_IN_JACK subtype
		0x01,				// EMBEDDED
		0x01,				// ID of this jack
		0,				// unused
//		0x06,				// unused

		// MIDI Adapter MIDI IN Jack Descriptor (External)
		6,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x02,				// MIDI_IN_JACK subtype
		0x02,				// EXTERNAL
		0x02,				// ID of this jack
		0,				// unused
//		0x07,				// unused

		// MIDI Adapter MIDI OUT Jack Descriptor (Embedded)
		9,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x03,				// MIDI_OUT_JACK subtype
		0x01,				// EMBEDDED
		0x03,				// ID of this jack
		0x01,				// number of input pins of this jack
		0x02,				// ID of the entity to which this pin is connected
		0x01,				// Output Pin number of the entity to which this input pin is connected
		0,				// unused
//		0x08,				// unused

		// MIDI Adapter MIDI OUT Jack Descriptor (External)
		9,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x03,				// MIDI_OUT_JACK subtype
		0x02,				// EXTERNAL
		0x04,				// ID of this jack
		0x01,				// number of input pins of this jack
		0x01,				// ID of the entity to which this pin is connected
		0x01,				// Output Pin number of the entity to which this input pin is connected
		0,				// unused
//		0x09,				// unused
#endif


#if USB_MIDI_NUM_PORTS >= 2
		// Second MIDI IN Jack Descriptor (Embedded)
		6,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x02,				// MIDI_IN_JACK subtype
		0x01,				// EMBEDDED
		0x05,				// ID of this jack
		0x00,				// unused

		// Second MIDI Adapter MIDI IN Jack Descriptor (External)
		6,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x02,				// MIDI_IN_JACK subtype
		0x02,				// EXTERNAL
		0x06,				// ID of this jack
		0x00,				// unused

		// Second MIDI Adapter MIDI OUT Jack Descriptor (Embedded)
		9,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x03,				// MIDI_OUT_JACK subtype
		0x01,				// EMBEDDED
		0x07,				// ID of this jack
		0x01,				// number of input pins of this jack
		0x06,				// ID of the entity to which this pin is connected
		0x01,				// Output Pin number of the entity to which this input pin is connected
		0x00,				// unused

		// Second MIDI Adapter MIDI OUT Jack Descriptor (External)
		9,				// Descriptor length
		CS_INTERFACE,			// Descriptor type (CS_INTERFACE)
		0x03,				// MIDI_OUT_JACK subtype
		0x02,				// EXTERNAL
		0x08,				// ID of this jack
		0x01,				// number of input pins of this jack
		0x05,				// ID of the entity to which this pin is connected
		0x01,				// Output Pin number of the entity to which this input pin is connected
		0x00,				// unused
#endif

		// Standard Bulk OUT Endpoint Descriptor
		9,				// Descriptor length
		DSCR_ENDPNT,			// Descriptor type
		MIDI_OUT_EP,				// Out Endpoint 2
		0x02,				// Bulk, not shared
		(uint8_t)(MIDI_DATA_MAX_PACKET_SIZE&0xff),	// num of bytes per packet (LSB)
		(uint8_t)(MIDI_DATA_MAX_PACKET_SIZE>>8),	// num of bytes per packet (MSB)
		0x00,				// ignore for bulk
		0x00,				// unused
		0x00,				// unused

		// Class-specific MS Bulk Out Endpoint Descriptor
		4+USB_MIDI_NUM_PORTS,	// Descriptor length
		CS_ENDPOINT,			// Descriptor type (CS_ENDPOINT)
		0x01,				// MS_GENERAL
		USB_MIDI_NUM_PORTS,	// number of embedded MIDI IN Jacks
		0x01,				// ID of embedded MIDI In Jack
#if USB_MIDI_NUM_PORTS >= 2
		0x05,				// ID of embedded MIDI In Jack
#endif

		// Standard Bulk IN Endpoint Descriptor
		9,				// Descriptor length
		DSCR_ENDPNT,			// Descriptor type
		MIDI_IN_EP,	// In Endpoint 1
		0x02,				// Bulk, not shared
		(uint8_t)(MIDI_DATA_MAX_PACKET_SIZE&0xff),	// num of bytes per packet (LSB)
		(uint8_t)(MIDI_DATA_MAX_PACKET_SIZE>>8),	// num of bytes per packet (MSB)
		0x00,				// ignore for bulk
		0x00,				// unused
		0x00,				// unused

		// Class-specific MS Bulk In Endpoint Descriptor
		4+USB_MIDI_NUM_PORTS,	// Descriptor length
		CS_ENDPOINT,			// Descriptor type (CS_ENDPOINT)
		0x01,				// MS_GENERAL
		USB_MIDI_NUM_PORTS,	// number of embedded MIDI Out Jacks
		0x03,				// ID of embedded MIDI Out Jack
#if USB_MIDI_NUM_PORTS >= 2
		0x07,				// ID of embedded MIDI Out Jack
#endif
};

__ALIGN_BEGIN uint8_t USB_Rx_Buffer   [DATA_OUT_PACKET_SIZE] __ALIGN_END ;

__ALIGN_BEGIN uint8_t USB_Tx_Buffer   [USB_TX_DATA_SIZE] __ALIGN_END ;

extern MIDI_IF_Prop_TypeDef MIDI_fops;

/* The interface class calbacks for the midi driver */
const USBD_Class_cb_TypeDef  USBD_MIDI_cb =
{
		usbd_midi_Init,
		usbd_midi_DeInit,
		NULL,
		NULL,
		NULL,
		usbd_midi_DataIn,
		usbd_midi_DataOut,
		usbd_midi_SOF,
		NULL,
		NULL,
		USBD_midi_GetCfgDesc,
};



static uint8_t  usbd_midi_Init        (void  *pdev, uint8_t cfgidx)
{
	UNUSED(cfgidx);
	/* Open the in EP */
	DCD_EP_Open(pdev,
			MIDI_IN_EP,
			DATA_IN_PACKET_SIZE,
			USB_OTG_EP_BULK);

	/* Open the out EP */
	DCD_EP_Open(pdev,
			MIDI_OUT_EP,
			DATA_OUT_PACKET_SIZE,
			USB_OTG_EP_BULK);

	/* Prepare Out endpoint to receive next packet */
	DCD_EP_PrepareRx(pdev,
			CDC_OUT_EP,
			(uint8_t*)(USB_Rx_Buffer),
			DATA_OUT_PACKET_SIZE);

	return USBD_OK;
}

static uint8_t  usbd_midi_DeInit(void  *pdev, uint8_t cfgidx)
{
	UNUSED(cfgidx);
	DCD_EP_Close(pdev,
			MIDI_IN_EP);
	DCD_EP_Close(pdev,
			MIDI_OUT_EP);
	return USBD_OK;
}


static uint8_t  usbd_midi_DataIn(void *pdev, uint8_t epnum)
{
	uint16_t tx_ptr;
	uint16_t tx_length;

	UNUSED(epnum);

	if (USB_Tx_State == 1)
	{
		if (USB_Tx_length == 0)
		{
			USB_Tx_State = 0;
		}
		else
		{
			if (USB_Tx_length > DATA_IN_PACKET_SIZE){
				tx_ptr = USB_Tx_ptr_out;
				tx_length = DATA_IN_PACKET_SIZE;

				USB_Tx_ptr_out += DATA_IN_PACKET_SIZE;
				USB_Tx_length -= DATA_IN_PACKET_SIZE;
			}
			else
			{
				tx_ptr = USB_Tx_ptr_out;
				tx_length = USB_Tx_length;

				USB_Tx_ptr_out += USB_Tx_length;
				USB_Tx_length = 0;
			}

			/* Prepare the available data buffer to be sent on IN endpoint */
			DCD_EP_Tx (pdev,
					MIDI_IN_EP,
					(uint8_t*)&USB_Tx_Buffer[tx_ptr],
					tx_length);
		}
	}

	return USBD_OK;
}

static uint8_t  usbd_midi_DataOut(void *pdev, uint8_t epnum){
	//	Should take care of processing the data and deliver it to the application.
	uint16_t USB_Rx_Cnt;
	/* Get the received data buffer and update the counter */
	USB_Rx_Cnt = ((USB_OTG_CORE_HANDLE*)pdev)->dev.out_ep[epnum].xfer_count;

	/* Forward the data to the user callback. */
	MIDI_fops.pIf_MidiRx(USB_Rx_Buffer, USB_Rx_Cnt);

	DCD_EP_PrepareRx(pdev,
			MIDI_OUT_EP,
			(uint8_t*)(USB_Rx_Buffer),
			DATA_OUT_PACKET_SIZE);

	return USBD_OK;
}

static uint8_t usbd_midi_SOF(void *pdev){
	Handle_USBAsynchXfer(pdev);

	return USBD_OK;
}


//static
void Handle_USBAsynchXfer (void *pdev)
{
	uint16_t tx_ptr;
	uint16_t tx_length;
	if(USB_Tx_State != 1)
	{
		// reached end of buffer
		if (USB_Tx_ptr_out == USB_TX_DATA_SIZE)
		{
			USB_Tx_ptr_out = 0;
		}

		if(USB_Tx_ptr_out == USB_Tx_ptr_in)
		{
			USB_Tx_State = 0;
			return;
		}

		if(USB_Tx_ptr_out > USB_Tx_ptr_in) /* rollback */
		{
			USB_Tx_length = USB_TX_DATA_SIZE - USB_Tx_ptr_out;

		}
		else
		{
			USB_Tx_length = USB_Tx_ptr_in - USB_Tx_ptr_out;

		}

		if (USB_Tx_length > DATA_IN_PACKET_SIZE)
		{
			tx_ptr = USB_Tx_ptr_out;
			tx_length = DATA_IN_PACKET_SIZE;

			USB_Tx_ptr_out += DATA_IN_PACKET_SIZE;
			USB_Tx_length -= DATA_IN_PACKET_SIZE;
		}
		else
		{
			tx_ptr = USB_Tx_ptr_out;
			tx_length = USB_Tx_length;

			USB_Tx_ptr_out += USB_Tx_length;
			USB_Tx_length = 0;
		}
		USB_Tx_State = 1;

		DCD_EP_Tx (pdev,
				MIDI_IN_EP,
				(uint8_t*)&USB_Tx_Buffer[tx_ptr],
				tx_length);
	}
}

static const uint8_t  *USBD_midi_GetCfgDesc (uint8_t speed, uint16_t *length)
{
	*length = sizeof (usbd_midi_CfgDesc);
	return usbd_midi_CfgDesc;
}
